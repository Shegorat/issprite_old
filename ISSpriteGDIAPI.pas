type
  TRGBQuad = packed record
    rgbRed        : Byte;
    rgbGreen      : Byte;
    rgbBlue       : Byte;
    rgbReserved   : Byte;
  end;
  PRGBQuad = ^TRGBQuad;

  TTextLog = packed record
    ImgWidth      : Longint;
    ImgHeight     : Longint;
    TextDC        : HDC;
    TextBits      : Pointer;
    ColorDC       : HDC;
    ColorBits     : Pointer;
    ShadowDC      : HDC;
    ShadowBits    : Pointer;
    GradientDC    : HDC;
    GradientBits  : Pointer;
    TempDC        : HDC;
    TempBits      : Pointer;
    TempDC2       : HDC;
    TempBits2     : Pointer;
    BitsCount     : Longint;
    Text          : PAnsiChar;
    TextLength    : Longint;
    TextRect      : TRect;
    TextFont      : HFONT;
    CharExtra     : Longint;
    NeedGradient  : Boolean;
    TextColor     : Cardinal;
    ShadowColor   : Cardinal;
    ShadowSize    : Longint;
    ShadowStyle   : DWORD;
    ShadowIntense : Byte;
  end;
  PTextLog = ^TTextLog;

  TPixelStruct0 = packed record
    x             : Longint;  //+00
    y             : Longint;  //+04
    dx            : Longint;  //+08
    dy            : Longint;  //+0C
    ImgWidth      : Longint;  //+10
    ImgHeight     : Longint;  //+14
    FirstBit      : Pointer;  //+18
    LastBit       : Pointer;  //+1C
    CurByte       : PRGBQuad; //+20
    mulR          : Byte;     //+24
    mulG          : Byte;     //+25
    mulB          : Byte;     //+26
    rgbA          : Byte;     //+27
    stride        : Longint;  //+28
    radius        : Longint;  //+2C
    ShadowIntense : Longint;  //+30
    ShadowColor   : Longint;	//+34
  end;
  PPixelStruct0 = ^TPixelStruct0;

function SwapByte(n: Integer): Byte;
begin
  if n>255 then Result:= 255 else Result:= n;
end;
{
procedure SetByte(ppxs: PPixelStruct0; dx, dy: Integer);
var
  rgb: PRGBQuad;
begin
  rgb:= Pointer(Longint(ppxs.CurByte)+((dy*ppxs.ImgWidth)+dx)shl 2);
  if (Longint(rgb)>=Longint(ppxs.FirstBit))and(Longint(rgb)<Longint(ppxs.LastBit))and(rgb^.rgbReserved<ppxs.rgbA) then
    PDWORD(rgb)^:= PDWORD(@ppxs.mulR)^;
end;
}
{$I 'ISSpriteGDIAsm'}

procedure FillBmiInfo(aWidth, aHeight: Integer; bmiInfo: PBitmapInfo);
begin
  with bmiInfo^.bmiHeader do begin
    biWidth:= aWidth;
    biHeight:= -aHeight;
    biPlanes:= 1;
    biBitCount:= 32;
    biCompression:= BI_RGB;
    biSize:= SizeOf(bmiInfo^.bmiHeader);
  end;
end;
{
current in ISSpriteGDIAsm.pas
procedure MaskBits(srcBits: PAnsiChar; dstBits: PAnsiChar; dstColor: TCOLORREF; BitCount: Integer);
var
  argb1, argb2: PRGBQuad;
  clr: Cardinal;
begin
  argb1:= Pointer(srcBits+(BitCount shl 2));
  argb2:= Pointer(dstBits+(BitCount shl 2));
  repeat
    Dec(BitCount);
    Dec(argb1);
    Dec(argb2);
    clr:= DWORD(argb1^) and not $FF000000;
    if clr<>$000000 then begin
      argb2^.rgbReserved:= (clr*$FF) div $FFFFFF;
      if argb2^.rgbReserved<>0 then begin
        argb2^.rgbRed:= TRGBQuad(dstColor).rgbRed*argb2^.rgbReserved div $FF;
        argb2^.rgbGreen:= TRGBQuad(dstColor).rgbGreen*argb2^.rgbReserved div $FF;
        argb2^.rgbBlue:= TRGBQuad(dstColor).rgbBlue*argb2^.rgbReserved div $FF;
      end;
    end;
  until (BitCount=0);
end;
}
{current in ISSpriteGDIAsm.pas
procedure BlendBits(srcBits, dstBits: PRGBQuad; Count: Integer);
label
  next;
begin
  repeat
    Dec(Count);

    if srcBits.rgbReserved=0 then goto next;

    dstBits.rgbRed:= srcBits.rgbRed + ((255-srcBits.rgbReserved)*dstBits.rgbRed div $FF);
    dstBits.rgbGreen:= srcBits.rgbGreen + ((255-srcBits.rgbReserved)*dstBits.rgbGreen div $FF);
    dstBits.rgbBlue:= srcBits.rgbBlue + ((255-srcBits.rgbReserved)*dstBits.rgbBlue div $FF);
    dstBits.rgbReserved:= srcBits.rgbReserved + ((255-srcBits.rgbReserved)*dstBits.rgbReserved div $FF);

next:
    Inc(srcBits);
    Inc(dstBits);
  until Count=0;
end;
}
procedure BlendRect(srcBits: PRGBQuad; srcX, srcY, srcW, srcH: Integer; dstBits: PRGBQuad; dstW, dstH: Integer);
var
  x, y, sx, sy: Integer;
  src, dst: PRGBQuad;
label
  next;
begin
  y:= 0;
  sy:= srcY;
  repeat
    if (sy > srcH) then break;

    x:= 0;
    sx:= srcX;
    dst:= PRGBQuad(Longint(dstBits) + (y*dstW)*SizeOf(TRGBQuad));
    src:= PRGBQuad(Longint(srcBits) + (sy*srcW + srcX)*SizeOf(TRGBQuad));
    repeat
      if (sx > srcW) then break;
      if (src^.rgbReserved = 0) then goto next;

      dst.rgbRed:= src.rgbRed + ((255-src.rgbReserved)*dst.rgbRed div $FF);
      dst.rgbGreen:= src.rgbGreen + ((255-src.rgbReserved)*dst.rgbGreen div $FF);
      dst.rgbBlue:= src.rgbBlue + ((255-src.rgbReserved)*dst.rgbBlue div $FF);
      dst.rgbReserved:= src.rgbReserved + ((255-src.rgbReserved)*dst.rgbReserved div $FF);

      next:
      Inc(dst);
      Inc(src);
      Inc(sX);
      Inc(x);
    until (x = dstW);

    Inc(sy);
    Inc(y);
  until (y = dstH);
end;

procedure SetGradient(aBits: PAnsiChar; aImgWidth, aImgHeight: Integer; aBeginColor, aEndColor: TCOLORREF; Flags: DWORD);
var
  x, y, n2: Integer;
  n1: PInteger;
  dR, dG, dB: Byte;
  clr: Cardinal;
  prgb: PRGBQuad;
begin
  dR:=  TRGBQuad(aEndColor).rgbRed-TRGBQuad(aBeginColor).rgbRed;
  dG:=  TRGBQuad(aEndColor).rgbGreen-TRGBQuad(aBeginColor).rgbGreen;
  dB:=  TRGBQuad(aEndColor).rgbBlue-TRGBQuad(aBeginColor).rgbBlue;
  prgb:= PRGBQuad(aBits+((aImgWidth*aImgHeight) shl 2));
  if Flags and $20 <> 0 then begin
    n1:= @y;
    n2:= aImgHeight;
  end else begin
    n1:= @x;
    n2:= aImgWidth;
  end;
  y:= aImgHeight;
  repeat
    Dec(y);
    x:= aImgWidth;
    repeat
      Dec(x);
      Dec(prgb);
      clr:= prgb^.rgbRed or (prgb^.rgbGreen shl 8) or (prgb^.rgbBlue shl 16);
      if (clr<>$000000) then begin
        if (prgb^.rgbReserved=0) then prgb^.rgbReserved:= (clr*$FF) div $FFFFFF;
        if prgb^.rgbReserved<>0 then begin
          prgb^.rgbRed:= (TRGBQuad(aBeginColor).rgbRed+MulDiv(n1^, dR, n2))*prgb^.rgbReserved div $FF;
          prgb^.rgbGreen:= (TRGBQuad(aBeginColor).rgbGreen+MulDiv(n1^, dG, n2))*prgb^.rgbReserved div $FF;
          prgb^.rgbBlue:= (TRGBQuad(aBeginColor).rgbBlue+MulDiv(n1^, dB, n2))*prgb^.rgbReserved div $FF;
        end;
      end;
    until (x=0);
  until (y=0);
end;
(*
cureent in ISSpriteGDIAsm.pas
procedure FillShadow(ppxs: PPixelStruct0);
var
  dr, r2, mx, dx, dy: Integer;
begin
  dr:= ppxs^.radius;
  repeat
    mx:= round(dr*sin(PI/4));
    dx:= -mx;
    r2:= dr*dr;
    if dr<>0 then
      ppxs^.rgbA:= SwapByte((255+ppxs.ShadowIntense*8) div dr{*(ppxs.radius+1-dr) div ppxs.radius});
    if (ppxs^.rgbA<>0) then begin
      ppxs^.mulR:= TRGBQuad(ppxs.ShadowColor).rgbRed*ppxs.rgbA div $FF;
      ppxs^.mulG:= TRGBQuad(ppxs.ShadowColor).rgbGreen*ppxs.rgbA div $FF;
      ppxs^.mulB:= TRGBQuad(ppxs.ShadowColor).rgbBlue*ppxs.rgbA div $FF;
      repeat
        dy:= RoundSqrt(r2, dx);
        //ShowMessage('dx: '+IntToStr(dx)+', dy: '+IntToStr(dy)+', rgbA: '+IntToStr(ppxs^.rgbA));
        SetByte(ppxs, dx, dy);
        SetByte(ppxs, dx, -dy);
        SetByte(ppxs, dy, dx);
        SetByte(ppxs, dy, -dx);
        inc(dx);
      until dx>mx;
    end;
    Dec(dr);
  until dr<0;
end;
*)
procedure Shadowing(TTL: PTextLog);
var
  z: Integer;
  rgb1, rgb2: PRGBQuad;
  ppxs: TPixelStruct0;
//qpc1, qpc2: TLargeInteger;
begin
//QueryPerformanceCounter(qpc1);
  with TTL^ do begin
    ppxs.ImgWidth:= ImgWidth;
    ppxs.ImgHeight:= ImgHeight;
    ppxs.LastBit:= Pointer(Longint(ShadowBits)+(BitsCount shl 2));
    ppxs.FirstBit:= ShadowBits;
    ppxs.radius:= ShadowSize;
    ppxs.stride:= BitsCount shl 2;
    ppxs.ShadowIntense:= ShadowIntense;
    ppxs.ShadowColor:= ShadowColor;
    if (ShadowStyle and $0080 <> 0) then begin
      rgb1:= Pointer(Longint(TempBits2)+(BitsCount shl 2));
      ppxs.CurByte:= ppxs.LastBit;
      ppxs.y:= BitsCount;
      repeat
        Dec(ppxs.y);
        Dec(rgb1);
        Dec(ppxs.CurByte);
        if (rgb1^.rgbReserved<>0) then
          FillShadow(@ppxs);
      until (ppxs.y=0);
    end
    else if (ShadowStyle and $0040 <> 0) then begin
      ppxs.x:= 0;
      repeat
        ppxs.y:= 0;
        repeat
          rgb1:= Pointer(Longint(TempBits2)+(ppxs.y*ImgWidth+ppxs.x) shl 2);
          if rgb1^.rgbReserved<>0 then begin
            z:= ShadowSize;
            repeat
              rgb2:= Pointer(Longint(ShadowBits)+((ppxs.y+z)*ImgWidth+(ppxs.x+z)) shl 2);
              if (Longint(rgb2)>=Longint(ShadowBits))and(Longint(rgb2)<Longint(ppxs.LastBit)) then begin
                rgb2^.rgbReserved:= SwapByte((255+ShadowIntense*8) div z);
                rgb2^.rgbRed:= TRGBQuad(ShadowColor).rgbRed*rgb2^.rgbReserved div $FF;
                rgb2^.rgbGreen:= TRGBQuad(ShadowColor).rgbGreen*rgb2^.rgbReserved div $FF;
                rgb2^.rgbBlue:= TRGBQuad(ShadowColor).rgbBlue*rgb2^.rgbReserved div $FF;
              end;
              Dec(z);
            until z=0;
          end;
          Inc(ppxs.y);
        until (ppxs.y>=ImgHeight);
        Inc(ppxs.x)
      until (ppxs.x>=ImgWidth);
    end;
  end;
//QueryPerformanceCounter(qpc2);
//ShowMessage('Shadowing time: '+IntToStr(qpc2-qpc1));
end;

procedure SmoothBits(TTL: PTextLog; dstBits: PRGBQuad);
var
  BitCount: Integer;
  rgb1, rgb2: PRGBQuad;
  LastBit: Longint;
  clr: array [0..3] of SmallInt;
//qpc1, qpc2: TLargeInteger;
begin
//QueryPerformanceCounter(qpc1);
  BitCount:= TTL^.BitsCount;
  rgb1:= PRGBQuad(TTL^.ShadowBits);
  LastBit:= Longint(TTL^.ShadowBits)+(BitCount shl 2);
  repeat
    Dec(BitCount);
    if rgb1^.rgbReserved<>0 then begin
      PDWORD(@clr[0])^:= 0;
      PDWORD(@clr[2])^:= 0;

      CalcPixel(rgb1, @clr);
      rgb2:= Pointer(Longint(rgb1)+4);
      if ((Longint(rgb2)>=Longint(TTL^.ShadowBits))and(Longint(rgb2)<LastBit)) then
        CalcPixel(rgb2, @clr);
      rgb2:= Pointer(Longint(rgb1)-4);
      if ((Longint(rgb2)>=Longint(TTL^.ShadowBits))and(Longint(rgb2)<LastBit)) then
        CalcPixel(rgb2, @clr);
      rgb2:= Pointer(Longint(rgb1)+(TTL^.ImgWidth shl 2));
      if ((Longint(rgb2)>=Longint(TTL^.ShadowBits))and(Longint(rgb2)<LastBit)) then
        CalcPixel(rgb2, @clr);
      rgb2:= Pointer(Longint(rgb1)+(-TTL^.ImgWidth shl 2));
      if ((Longint(rgb2)>=Longint(TTL^.ShadowBits))and(Longint(rgb2)<LastBit)) then
        CalcPixel(rgb2, @clr);

      dstBits^.rgbRed:= clr[0] div 5;
      dstBits^.rgbGreen:= clr[1] div 5;
      dstBits^.rgbBlue:= clr[2] div 5;
      dstBits^.rgbReserved:= clr[3] div 5;
    end;
    Inc(dstBits);
    Inc(rgb1);
  until BitCount=0;
  asm
    emms
  end;
//QueryPerformanceCounter(qpc2);
//ShowMessage('SmoothBits time: '+IntToStr(qpc2-qpc1));
end;

procedure DrawShadowText(LogText: PTextLog);
var
  Clr: Cardinal;
  Bits: Pointer;
begin
  ClearBits(LogText^.TempBits, LogText^.BitsCount);
  DrawText(LogText^.TempDC, LogText^.Text, LogText^.TextLength, LogText^.TextRect, DT_NOCLIP);

  if LogText^.ShadowSize <> 0 then
    MaskBits(LogText^.TempBits, LogText^.TempBits2, $FFFFFF, LogText^.BitsCount);

  if LogText^.NeedGradient then begin
    clr:= $FFFFFF;
    Bits:= LogText^.GradientBits;
  end else begin
    clr:= LogText^.TextColor;
    Bits:= LogText^.ColorBits;
  end;
	
  MaskBits(LogText^.TempBits, Bits, Clr, LogText^.BitsCount);
end;

procedure CreateNewSurface(srcDC: HDC; var dstDC: HDC; var dstBits: Pointer; var dstBmp1, dstBmp2: HBITMAP; ImgSize: PTextLog; MapSize: PSize);
var
  bmiInfo: TBitmapInfo;
begin
  dstDC:= CreateCompatibleDC(srcDC);
  FillBmiInfo(ImgSize^.ImgWidth, ImgSize^.ImgHeight, @bmiInfo);
  dstBmp1:= CreateDIBSection(srcDC, bmiInfo, DIB_RGB_COLORS, dstBits, 0, 0);
  dstBmp2:= SelectObject(dstDC, dstBmp1);
  ClearBits(dstBits, ImgSize^.BitsCount);
  SetBkMode(dstDC, TRANSPARENT);
  if MapSize<>nil then begin
    SetMapMode(dstDC, MM_ANISOTROPIC);
    SetWindowExtEx(dstDC, MapSize^.cx*20, MapSize^.cy*20, nil);
    SetViewportExtEx(dstDC, MapSize^.cx, MapSize^.cy, nil);
  end;
end;

function InRect(const P: TPoint; const R: TRect): Boolean;
begin
  Result:= ((P.x >= R.Left) and (P.x <= R.Right))
  and      ((P.y >= R.Top) and (P.y <= R.Bottom));
end;