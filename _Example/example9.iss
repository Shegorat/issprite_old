#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: callbackctrl.dll; DestDir: {tmp}; Flags: dontcopy
Source: KrinkelsTeam.png; DestDir: {tmp}; Flags: dontcopy
Source: 5.png; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';
function ReleaseCapture(): Longint; external 'ReleaseCapture@user32.dll stdcall';

var
  flag: Boolean;
var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer; tmr: Longword;

var
  fnt: Longword;

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
var
  n: Integer;
begin
  n:= spImgGetTransparent(sp1);
  if Flag then begin
    n:=n+5;
    if n>=255 then begin
      n:= 255;
      flag:=false;
    end;
  end else begin
    n:=n-5;
    if n<=0 then begin
      n:= 0;
      flag:= true;
    end;
  end;
  log(IntToStr(n));
  spImgSetTransparent(sp1, n);
  spApplyChanges(WizardForm.Handle);
end;

procedure WizardFormOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(WizardForm.Handle,$0112,$F012,0);
end;

procedure InitializeWizard();
var
  l: Longword;
begin
  ExtractTemporaryFile('ISSprite.dll');

  spInitialize(true, true);
  
  ExtractTemporaryFile('KrinkelsTeam.png');
  ExtractTemporaryFile('5.png');

  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;
  WizardForm.DoubleBuffered:= false;

  WizardForm.BorderStyle:= bsNone;
  WizardForm.Color:= $FFFF00;
  WizardForm.Width:= 500;
  WizardForm.OnMouseDown:= @WizardFormOnMouseDown;

  Sp1:= spImgLoadImage(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\Krinkelsteam.png')), 0, 0, 500, 258, True, True);
  Sp2:= spImgLoadImage(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\5.png')), 80, 260, 100, 100, True, False);

  fnt:= spFntCreateFont(PAnsiChar(WizardForm.Font.Name), 0, false, false, false, 10);
  l:= spShdAddText(WizardForm.Handle, 0, 240, ScaleX(425), ScaleY(70), '���� ���� ����� ����������� � [b][min=200][color=$FF0000]'+WizardForm.DirEdit.Text+'\Local\AppData\Server host[/color][/b][/min] � ��� ��������� ����������� [i][color=$FF0000]9.98 ��[/color][/i] ���������� ��������� ������������.', $EEEEEE, fnt);
  spShdSetShadow(l, 1, 3, $AAAAAA, SHD_STYLESHADOW);
  
  spShdAddGradient(l, $0000FF, $FF00FF, $10);

  spApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 80, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  spFntFreeFont(fnt);
  spShutdown;
end;