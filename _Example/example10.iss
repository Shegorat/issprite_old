#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: innocallback.dll; DestDir: {tmp}; Flags: dontcopy
Source: KrinkelsTeam.png; DestDir: {tmp}; Flags: dontcopy
Source: 5.png; DestDir: {tmp}; Flags: dontcopy
Source: 7f77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: 7sf77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallback@files:innocallback.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';
function ReleaseCapture(): Longint; external 'ReleaseCapture@user32.dll stdcall';

procedure WizardFormOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(WizardForm.Handle,$0112,$F012,0);
end;

var
  Sp1, sp2, sp3, sp4: Longword;
  n2, n3: integer; tmr: Longword;

var
  fnt: Longword;
  l: Longword;

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
begin

  MoveSpriteHor(SP3, 2, 25, 472);
  spImgSetSpriteIndex(SP3, n2);

  MoveSpriteHor(SP4, 2, 25, 472);
  spImgSetSpriteIndex(SP4, n2);

  n2:=n2+1; if n2=16 then n2:=1;
  n3:=n3+1; if n3=16 then n3:=1;
  spApplyChanges(WizardForm.handle);
  spImgUpdateImageForm(WizardForm.Handle);
end;

procedure InitializeWizard();
begin
  spInitialize();
  
  ExtractTemporaryFile('KrinkelsTeam.png');
  ExtractTemporaryFile('5.png');
  ExtractTemporaryFile('7f77.bmp');
  ExtractTemporaryFile('7sf77.bmp');

  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;
  WizardForm.BorderStyle:= bsNone;
  WizardForm.Width:= 500;
  WizardForm.OnMouseDown:= @WizardFormOnMouseDown;


  Sp2:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\5.png')), 80, 260, 128, 128, -1, True, False);
  Sp3:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7f77.bmp')), 30, 6, 28, 40, $FF00FF, False, False);
  Sp4:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7sf77.bmp')), 120, -3, 48, 53, $FF00FF, False, False);
  Sp1:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\Krinkelsteam.png')), 0, 0, 500, 258, -1, True, True);
  spImgSetSpriteCount(SP3, 15);
  spImgSetSpriteCount(SP4, 15);

  spImgCreateFormFromImage(WizardForm.Handle, SP1);

  fnt:= spFntCreateFont(PAnsiChar(WizardForm.Font.Name), false, false, false, false, 15);
  l:= spShdAddText(WizardForm.Handle, 0, 240, ScaleX(425), ScaleY(70), '���� ���� ����� ����������� � [b][min=200][color=$FF0000]'+WizardForm.DirEdit.Text+'[/color][/b][/min] � ��� ��������� ����������� [i][color=$FF0000]9.98 ��[/color][/i] ���������� ��������� ������������.', 4, 1, $000000, $AAAAAA, fnt);
  spShdAddGradient(l, $0000FF, $FF00FF, $10);

  spApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 80, WrapTimerProc(@OnTimer, 4));
end;

procedure CurPageChanged(CurPageID: Integer);
begin
  case CurPageID of
    wpSelectDir: begin
        spShdSetText(l, 'lalala');
      //shdFreeGradient(h)
      //shdSetVisible1(l, False);
    end;
  end;
  
end;

procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  spFntFreeFont(fnt);
  spShutdown;
end;