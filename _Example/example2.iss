#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: innocallback.dll; DestDir: {tmp}; Flags: dontcopy
Source: 6766.bmp; DestDir: {tmp}; Flags: dontcopy
Source: a4s.bmp; DestDir: {tmp}; Flags: dontcopy
Source: gpl-2.0.txt; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TWindowProc = function(h: HWND; Msg, wParam, lParam: Longword): Longword;
  
  TScrollInfo = record
    cbSize    : UINT;
    fMask     : UINT;
    nMin      : Integer;
    nMax      : Integer;
    nPage     : UINT;
    nPos      : Integer;
    nTrackPos : Integer;
  end;

const
  SB_VERT = 1;
  SIF_POS = 4;
  WM_VSCROLL = $0115;
  WM_MOUSEWHEEL = $020A;
  
function WrapWindowProc(Callback: TWindowProc; ParamCount: Integer): Longword; external 'wrapcallback@files:innocallback.dll stdcall';
function CallWindowProc(lpPrevWndFunc: Longint; hWnd: HWND; Msg: UINT; wParam: Longint; lParam: Longint): Longint; external 'CallWindowProcA@user32.dll stdcall';
function SetWindowLong(hWnd: HWND; nIndex: Integer; dwNewLong: Longint): Longint; external 'SetWindowLongA@user32.dll stdcall';
function GetScrollInfo(hwnd: HWND; fnBar: integer; var lpsi: TScrollInfo): Boolean; external 'GetScrollInfo@user32.dll stdcall';

var
  SP1, SP2: Longword;
  LicenseMemo: TMemo;
  OldProc: Longint;

function MemoScroll(h: HWND; Msg, wParam, lParam: Longword): Longword;
var
  lpsi: TScrollInfo;
  k, y, x: Integer;
begin
  Result:= CallWindowProc(OldProc, h, Msg, wParam, lParam);
  if (Msg= WM_VSCROLL)or(Msg=WM_MOUSEWHEEL) then begin
    lpsi.cbSize:= SizeOf(lpsi);
    lpsi.fMask:= SIF_POS;
    
    GetScrollInfo(h, SB_VERT, lpsi);
    
    k:= lpsi.nPos;
    y:= spImgGetSpriteCount(SP2);

    if k>y then begin
      x:= Trunc(k/y);
      if ((k mod y)=0) then x:=x-1;
      k:= k- (x*y);
    end;

    spImgSetSpriteIndex(SP2, k);
    spApplyChanges(WizardForm.Handle);
  end;
end;
  
procedure InitializeWizard();
begin
  ExtractTemporaryFile('6766.bmp');
  ExtractTemporaryFile('a4s.bmp');
  ExtractTemporaryFile('gpl-2.0.txt');
  
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;

  spInitialize;

  LicenseMemo:= TMemo.Create(WizardForm);
  LicenseMemo.SetBounds(60, 60, 340, 200);
  licenseMemo.Parent:= WizardForm;
  LicenseMemo.BorderStyle:= bsSingle;
  LicenseMemo.ScrollBars:= ssBoth;
  LicenseMemo.Lines.LoadFromFile(ExpandConstant('{tmp}\gpl-2.0.txt'));

  OldProc:= SetWindowLong(LicenseMemo.Handle, -4, WrapWindowProc(@MemoScroll, 4));
  
  Sp1:= spImgLoadImage(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\6766.bmp')), 0, 0, 497, 360, -1, True, True);
  SP2:= spImgLoadImage(WizardForm.Handle, PAnsiChar(ExpandConstant('{tmp}\a4s.bmp')), 420, 10, 69, 116, $FF00FF, False, True);
  spImgSetSpriteCount(SP2, 14);
  spApplyChanges(WizardForm.Handle);
end;


procedure DeinitializeSetup();
begin
  SetWindowLong(LicenseMemo.Handle, -4, OldProc);
  spShutdown;
end;
