#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: callbackctrl.dll; DestDir: {tmp}; Flags: dontcopy
Source: KrinkelsTeam.png; DestDir: {tmp}; Flags: dontcopy
Source: 5.png; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;
Source: button.png; DestDir: {tmp}; Flags: dontcopy
Source: Box.png; DestDir: {tmp}; Flags: dontcopy

[code]
function ReleaseCapture(): Longint; external 'ReleaseCapture@user32.dll stdcall';

var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer;
  h1, h2, h3: HWND;
  fnt: Longword;

procedure btnOnClick(hBtn: HWND; nEventID: Word);
begin
  //MsgBox('Button Clicked', mbConfirmation, MB_OK);
  spBtnSetVisible(h3, not spBtnGetVisible(h3));
end;

procedure WizardFormOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(WizardForm.Handle,$0112,$F012,0);
end;
procedure InitializeWizard();
var
  l: Longword;
begin
  ExtractTemporaryFile('ISSprite.dll');
  spInitialize(true, true);
  
  ExtractTemporaryFile('KrinkelsTeam.png');
  ExtractTemporaryFile('5.png');
  ExtractTemporaryFile('button.png');
  ExtractTemporaryFile('box.png');
  
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;
  WizardForm.BorderStyle:= bsNone;
  WizardForm.Color:= $FFFF00;
  WizardForm.Width:= 500;
  WizardForm.OnMouseDown:= @WizardFormOnMouseDown;

  Sp1:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\Krinkelsteam.png')), 0, 0, 500, 258, True, True);
  //Sp2:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\5.png')), 80, 260, 100, 100, True, False);

  spImgCreateImageForm(WizardForm.Handle, true);

  fnt:= spFntCreateFont(PAnsiChar(WizardForm.Font.Name), 0, false, false, false, 10);
  l:= spShdAddText(WizardForm.Handle, 0, 240, ScaleX(425), ScaleY(70), '���� ���� ����� ����������� � [b][min=200][color=$FF0000]'+WizardForm.DirEdit.Text+'\Local\AppData\Server host[/color][/b][/min] � ��� ��������� ����������� [i][color=$FF0000]9.98 ��[/color][/i] ���������� ��������� ������������.', $EEEEEE, fnt);
  spShdAddGradient(l, $0000FF, $FF00FF, $10);

  h1:= spBtnCreateButton(WizardForm.Handle, PChar(ExpandConstant('{tmp}\button.png')), 20, 200, 120, 32, $20, 12);
  spBtnSetEvent(h1, btnMouseClickEvent, @btnOnClick);
  spBtnSetFontColor(h1, $FFFFFF, $FF6666, $6666FF, $888888);
  spBtnSetText(h1, 'TestButton');

  spBtnSetCursor(h1, spBtnGetSysCursor(32649));

  h2:= spBtnCreateButton(WizardForm.Handle, PChar(ExpandConstant('{tmp}\box.png')), 150, 210, 16, 16, $80, 0);
  
  h3:= spBtnCreateButton(WizardForm.Handle, PChar(ExpandConstant('{tmp}\5.png')), 80, 180, 128, 128, BTN_PANEL, 0);
  
  spBtnSetVisible(h3, false);
  spBtnSetTransparent(h3, 250);
  spBtnSetChecked(h2, 1);

  spApplyChanges(WizardForm.handle);
end;


procedure DeinitializeSetup();
begin
  spFntFreeFont(fnt);
  spShutdown;
end;