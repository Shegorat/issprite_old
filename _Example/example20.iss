#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
RawDataResource=Img1:6766.bmp|Img2:7f77.bmp|Img3:7sf77.bmp
OutputDir=.


[Files]
Source: callbackctrl.dll; DestDir: {tmp}; Flags: dontcopy
//Source: 6766.bmp; DestDir: {tmp}; Flags: dontcopy
//Source: 7f77.bmp; DestDir: {tmp}; Flags: dontcopy
//Source: 7sf77.bmp; DestDir: {tmp}; Flags: dontcopy
Source: ResourceStub.dll; DestDir: {tmp}; Flags: dontcopy;
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';

var
  Sp1, sp2, sp3: Longword;
  n2, n3: integer; tmr: Longword;

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
begin
  MoveSpriteHor(SP2, 2, 25, 472);
  spImgSetSpriteIndex(SP2, n2);

  MoveSpriteHor(SP3, 2, 25, 472);
  spImgSetSpriteIndex(SP3, n2);
  
  n2:=n2+1; if n2=16 then n2:=1;
  n3:=n3+1; if n3=16 then n3:=1;
  spApplyChanges(WizardForm.handle);
end;

procedure InitializeWizard();
begin
  ExtractTemporaryFile('ISSprite.dll');

  spInitialize(true, true);
  
//  ExtractTemporaryFile('6766.bmp');
//  ExtractTemporaryFile('7f77.bmp');
//  ExtractTemporaryFile('7sf77.bmp');
  
  ExtractTemporaryFile('ResourceStub.dll');
  
  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;

  with TLabel.Create(WizardForm) do begin
    with Font do begin
      Color:=clRed;
      Style:=[fsBold, fsItalic];
      Size:=14;
    end;
    Caption:='TEST Label';
    Left:=150;
    Top:=25;
    Parent:=WizardForm;
    Transparent:=True;
  end;
  WizardForm.DirEdit.Parent:= WizardForm;
  WizardForm.DirEdit.BorderStyle:= bsNone;
  WizardForm.DirEdit.Left:= 108;
  WizardForm.DirEdit.Top:= 74;

  //Sp1:= ImgLoadfromBuffer(WizardForm.Handle, PChar(ExpandConstant('{tmp}\6766.bmp')), 0, 0, 497, 360, True, True);
  //Sp2:= ImgLoadfromBuffer(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7f77.bmp')), 30, 6, 28, 40, False, False);
  //Sp3:= ImgLoadfromBuffer(WizardForm.Handle, PChar(ExpandConstant('{tmp}\7sf77.bmp')), 120, -3, 48, 53, False, False);
  
  log(IntToStr(HInstance));
  Sp1:= spImgLoadImageFromResourceName(WizardForm.Handle, hInstance, '_IS_IMG1', 'RT_RCDATA', 0, 0, 497, 360, True, True);
  //Sp1:= spImgLoadImageFromResourceLibrary(WizardForm.Handle, ExpandConstant('{tmp}\ResourceStub.dll'), 'IMG1', 'RT_RCDATA', 0, 0, 497, 360, True, True);
  Sp2:= spImgLoadImageFromResourceLibrary(WizardForm.Handle, ExpandConstant('{tmp}\ResourceStub.dll'), 'IMG2', 'RT_RCDATA', 30, 6, 28, 40, False, False);
  Sp3:= spImgLoadImageFromResourceLibrary(WizardForm.Handle, ExpandConstant('{tmp}\ResourceStub.dll'), 'IMG3', 'RT_RCDATA', 120, -3, 48, 53, False, False);
  
  spImgSetBackgroundColor(SP2, $FF00FF);
  spImgSetBackgroundColor(SP3, $FF00FF);
  spImgSetSpriteCount(SP2, 15);
  spImgSetSpriteCount(SP3, 15);
  spApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 60, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  spShutdown;
end;
