#include "issprite.iss"
[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp
OutputDir=.

[Files]
Source: callbackctrl.dll; DestDir: {tmp}; Flags: dontcopy
Source: KrinkelsTeam.png; DestDir: {tmp}; Flags: dontcopy
Source: 5.png; DestDir: {tmp}; Flags: dontcopy
Source: ISSprite.dll; DestDir: {tmp}; Flags: dontcopy;

[code]
type
  TTimerProc = procedure(HandleW, Msg, idEvent, TimeSys: LongWord);

function WrapTimerProc(callback: TTimerProc; Paramcount: Integer): longword; external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';
function SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc: LongWord): longword; external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd, nIDEvent: LongWord): LongWord; external 'KillTimer@user32.dll stdcall';
function ReleaseCapture(): Longint; external 'ReleaseCapture@user32.dll stdcall';

procedure OnTimer(HandleW, Msg, idEvent, TimeSys: LongWord);
begin
  spImgUpdateImageForm(WizardForm.Handle);
end;

procedure WizardFormOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(WizardForm.Handle,$0112,$F012,0);
end;

var
  Sp1, sp2: Longword;
  tmr: Longword;

  
procedure InitializeWizard();
var
  l: Longword;
begin
  ExtractTemporaryFile('ISSprite.dll');
  
  spInitialize(true, true);
  
  ExtractTemporaryFile('KrinkelsTeam.png');
  ExtractTemporaryFile('5.png');

  WizardForm.InnerNotebook.Hide;
  WizardForm.OuterNotebook.Hide;
  WizardForm.BorderStyle:= bsNone;
  WizardForm.Color:= $FFFF00;
  WizardForm.Width:= 500;
  WizardForm.OnMouseDown:= @WizardFormOnMouseDown;

  Sp1:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\Krinkelsteam.png')), 0, 0, 500, 258, True, True);
  Sp2:= spImgLoadImage(WizardForm.Handle, PChar(ExpandConstant('{tmp}\5.png')), 80, 260, 100, 100, True, False);

  spImgCreateImageForm(WizardForm.Handle, false);
  //spImgCreateFormFromImage(WizardForm.Handle, SP1, false);

  spApplyChanges(WizardForm.handle);
  tmr:= SetTimer(0, 0, 80, WrapTimerProc(@OnTimer, 4));
end;


procedure DeinitializeSetup();
begin
  KillTimer(0, Tmr);
  spShutdown;
end;