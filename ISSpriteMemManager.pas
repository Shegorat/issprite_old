const
  soFromBeginning = 0;
  soFromCurrent   = 1;
  soFromEnd       = 2;

  MEM_MAX_COUNT = 1024;

type
  TMemManager = packed record
    CRC32: Cardinal;
    PData: Pointer;
  end;
  PMemManager = ^TMemManager;

var
  CRCtable: array[0..255] of Cardinal;
  MemImgCount: Longword;
  PMemTable: Pointer;

procedure CRC32Init();
var
  c: cardinal;
  i, j: integer;
begin
  MemImgCount:= MEM_MAX_COUNT;
  i:=0;
  repeat
    c := i;
    j:= 1;
    repeat
      if odd(c) then
        c := (c shr 1) xor $EDB88320
      else
        c := (c shr 1);
      Inc(j);
    until j=9;
    CRCtable[i] := c;
    Inc(i);
  until i=256;
end;

function UpdateCRC(OldCRC: cardinal; StPtr: pointer; StLen: integer): cardinal;
asm
  test edx,edx;
  jz @ret;
  neg ecx;
  jz @ret;
  sub edx,ecx;

  push ebx;
  xor ebx,ebx;
@next:
  mov bl,al;
  xor bl,byte [edx+ecx];
  shr eax,8;
  xor eax,cardinal [CRCtable+ebx*4];
  inc ecx;
  jnz @next;
  pop ebx;

@ret:
end;	

function BufferCRC(const pBuff: Pointer; BufLen: Integer): Cardinal;
const
	BlockSize = 1024 * 64;	
var
	pBlock: PAnsiChar;
	Elapse, c: Integer;
	crc: Cardinal;
begin
	crc:= $FFFFFFFF;
	Elapse:= BufLen;
	pBlock:= pBuff;
	repeat
		c:= Elapse mod (BlockSize+1);
    if c=0 then c:= BlockSize;
		crc:= UpdateCRC(crc, pBlock, c);
		Dec(Elapse, c);
		if Elapse<0 then Break;
		Inc(pBlock, c);
	until Elapse<=0;
	Result:= crc;
end;

function FileCRC(fName: PAnsiChar): Cardinal;
var
	hFile, crc: THandle;
	Buff: Pointer;
	FileSize, r: Integer;
begin
	hFile:= CreateFile(fName, GENERIC_READ, FILE_SHARE_READ, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	FileSize:= SetFilePointer(hFile, 0, nil, soFromEnd);
  SetFilePointer(hFile, 0, nil, soFromBeginning);
	Buff:= VirtualAlloc(nil, FileSize, MEM_RESERVE or MEM_COMMIT, PAGE_READWRITE);
  ReadFile(hFile, Buff^, FileSize, LongWord(r), nil);
  SetFilePointer(hFile, 0, nil, soFromBeginning);
	CloseHandle(hFile);
	crc:= BufferCRC(Buff, FileSize);
	VirtualFree(Buff, 0, MEM_RELEASE);
  Result:= crc;
end;

function MemGetImg(crc: Cardinal): Pointer;
var
  MM: PMemManager;
  i: Longword;
begin
  Result:= nil;
  i:= MEM_MAX_COUNT-1;
  repeat
    MM:= PMemManager(Longword(PMemTable)+i*SizeOf(TMemManager));
    if crc=MM^.CRC32 then begin
      Result:= MM^.PData;
      Break;
    end;
    Dec(i);
  until i<MemImgCount;
end;

function AddMemInfo(CRC: Longword; Data: Pointer): Boolean;
var
  MM: PMemManager;
begin
  Result:= False;
  if MemImgCount=0 then Exit;

  Dec(MemImgCount);
  MM:= PMemManager(Longword(PMemTable)+MemImgCount*Sizeof(TMemManager));
  MM^.CRC32:= CRC;
  MM^.PData:= Data;
  Result:= True;
end;