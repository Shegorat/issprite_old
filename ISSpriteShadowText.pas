const
  TAG_B     : array [0..1] of AnsiChar = 'b'#0;
  TAG_I     : array [0..1] of AnsiChar = 'i'#0;
  TAG_U     : array [0..1] of AnsiChar = 'u'#0;
  TAG_S     : array [0..1] of AnsiChar = 's'#0;
  TAG_COLOR : array [0..5] of AnsiChar = 'color'#0;
  TAG_MIN   : array [0..3] of AnsiChar = 'min'#0;
  TAG_FONT  : array [0..4] of AnsiChar = 'font'#0;
  TAG_SIZE  : array [0..4] of AnsiChar = 'size'#0;

  TAG_ALIGN : array [0..5] of AnsiChar = 'align'#0;
  TAG_TAB   : array [0..3] of AnsiChar = 'tab'#0;

  TG_ALIGN  : array [0..6] of AnsiChar = '[align'#0;

  AL_LEFT   : array [0..6] of AnsiChar = 'taleft'#0;
  AL_CENTER : array [0..8] of AnsiChar = 'tacenter'#0;
  AL_RIGHT  : array [0..7] of AnsiChar = 'taright'#0;

  CHR_SLASH : AnsiChar = '/';
  CHR_LROUND: AnsiChar = '[';
  CHR_RROUND: AnsiChar = ']';
  CHR_EQUAL : AnsiChar = '=';
  CHR_ENDLC : array [0..2] of AnsiChar = '%n';
  CHR_ENDLH : array [0..2] of AnsiChar = #13#10;
  CHR_SPACE : array [0..1] of AnsiChar = ' '#0;

  MAX_REPEAT = 10;

function GetTag(StartPos: Integer; var S, Name, Val, Tags: String): Integer;
var
  p1, p2, p3: Integer;
  tg: String;
begin
  p1:=StartPos; p2:= StartPos+1; p3:= StartPos+1;
  while (p1>0)and(s[p1]<>CHR_LROUND) do Dec(p1);
  while (s[p2]<>CHR_RROUND) do Inc(p2);
  while (s[p3]<>CHR_EQUAL)and(p3<p2) do Inc(p3);
  Name:= copy(S, p1+1, p3-2);
  Val:= copy(S, p3+1, p2-p3-1);
  tg:= Copy(S, p1, p2-p1+1);
  if pos(tg, Tags)<=0 then
    Tags:= tags+tg;
  Delete(S, p1, p2-p1+1);
  Result:= Length(tg);
end;

procedure DeleteTags(var S, Tags: String);
var
  p1, p2, p3: Integer;
begin
  repeat
    if (s='')or(s[1]<>CHR_LROUND)or((s[1]=CHR_LROUND)and(s[2]<>CHR_SLASH)) then Break;
    p1:=2; while s[p1]<>CHR_RROUND do Inc(p1);
    p2:= pos(CHR_LROUND+copy(s, 3, p1-3), tags);
    p3:= p2; while tags[p3]<>CHR_RROUND do Inc(p3);
    Delete(tags, p2, p3-p2+1);
    Delete(s, 1, p1);
  until Tags='';
end;

function ExpandTag(const TagName, TagVal: String; lpFnt: PLogFont; MinLen: PInteger; TmpColor: PCardinal): Boolean;
begin
  Result:= True;
  case AnsiIndexText(AnsiLowercase(TagName), [@TAG_B, @TAG_I, @TAG_U, @TAG_S, @TAG_COLOR, @TAG_MIN, @TAG_FONT, @TAG_SIZE, @TAG_ALIGN]) of
    0:  begin
          if TagVal<>'' then
            lpFnt^.lfWeight:= StrToInt(TagVal)
          else
            lpFnt^.lfWeight:= FW_BOLD;
        end;
    1:  lpFnt^.lfItalic:= 1;
    2:  lpFnt^.lfUnderline:= 1;
    3:  lpFnt^.lfStrikeOut:= 1;
    4:  if TmpColor<>nil then TmpColor^:= StrToInt(TagVal);
    5:  if MinLen<>nil then MinLen^:= StrToInt(TagVal);
    6:  strcopy(lpFnt^.lfFaceName, PAnsiChar(TagVal), strlen(PAnsiChar(TagVal)));
			//StrLCopy(lpFnt^.lfFaceName, PAnsiChar(TagVal), Length(TagVal));    
    7:  begin
          if ISSpriteMain.spNeedLPY then
            lpFnt^.lfHeight:= -MulDiv(StrToInt(TagVal), ISSpriteMain.LPixelsY, 72)*20
          else
            lpFnt^.lfHeight:= -StrToInt(TagVal)*20;
        end;
    8: ;
  else
    Result:= False;
  end;
end;

function ForwardTag(const S: String): Integer;
var
  i, l, k, n1, n2: Integer;
  tname: String;
Label
  Start, Ret;
begin
  i:= 1;
  l:= Length(S);
  repeat
Start:
    if s[i]=CHR_LROUND then begin
      k:= i+1;
      repeat
        if (S[k]=CHR_LROUND)or(S[k]=CHR_RROUND) then
          Break;
        Inc(k);
      until k>l;
      if S[k]=CHR_LROUND then begin
        i:= k;
        goto Start;
      end;
      if (k-i)>0 then begin
        n1:= i+1;
        if S[n1]=CHR_SLASH then Inc(n1);
        n2:= n1;
        while (n2<k)and(s[n2]<>CHR_EQUAL)and(s[n2]<>CHR_RROUND) do
          Inc(n2);
        tname:= copy(s, n1, n2-n1);
        if AnsiIndexText(AnsiLowercase(tname), [@TAG_B, @TAG_I, @TAG_U, @TAG_S, TAG_COLOR, TAG_MIN, TAG_FONT, TAG_SIZE, TAG_ALIGN, TAG_TAB])<0 then begin
          i:= k;
          goto Start;
        end else
          goto Ret;
      end;
    end;
    Inc(i);
  until i>l;

Ret:
  if i>l then Inc(i);
  Result:= i;
end;

procedure GetTextWidthEx(aDC: HDC; const Origin: String; lpFont: HFONT; MaxWidth: Integer; OutLen, OutWidth, OutHeight: PInteger);
var
  tw, k, n, nl, l, ol, space: Integer;
  tags, name, val, cur, S: String;
  lfnt: TLogFont; fnt: HFONT;
  Stop: Boolean;
  MinLen: TSize;
begin
  S:= Origin; OutLen^:= 0; OutWidth^:= 0; OutHeight^:= 0; Stop:= False;
  Space:= GetTextWidth(aDC, lpFont, CHR_SPACE);
  repeat
    k:= Pos(CHR_LROUND, S); tags:= ''; minlen.cx:=0;
    FillChar(lfnt, SizeOf(TLogFont), 0); GetObject(lpFont, SizeOf(TLogFont), @lfnt);
    if k=1 then
      repeat
        if (s='')or(s[1]<>CHR_LROUND)or(s[2]=CHR_SLASH) then Break;
        l:= GetTag(1, S, Name, Val, Tags);
        if Name=TAG_TAB then begin
          if Val<>'' then
            OutWidth^:= OutWidth^+StrToInt(Val)*Space
          else
            OutWidth^:= OutWidth^+5*Space;
          Delete(Tags, Pos(TAG_TAB, Tags)-1, l);
        end else
        ExpandTag(name, val, @lfnt, @minlen.cx, nil);
      until k<>1;
    k:= Pos(CHR_LROUND, S);
    if k<=0 then k:= Length(S)+1;
    nl:= Pos(CHR_ENDLC, S); n:= Pos(CHR_ENDLH, S);
    if (n>0)and((n<nl)or(nl<=0)) then nl:= n;
    if (nl>0)and(nl<k) then k:=nl;
    fnt:= CreateFontIndirect(lfnt);
    cur:= Copy(S, 1, k-1);
    l:= Length(cur);
    DPToLP(aDC, MinLen, 1);
    if (MinLen.cx>0) then Cur:= MinimizePathName(aDC, cur, fnt, minlen.cx);
    tw:= GetTextWidth(aDC, fnt, PAnsiChar(cur));
    if (OutWidth^+tw>MaxWidth)and(l>3)and(MinLen.cx=0) then
      repeat
        k:= FindSpace(PAnsiChar(S), 1, k-1);
        if (k<=0)or(TrimEx(cur)='') then Break;
        cur:= copy(S, 1, k-1);
        tw:= GetTextWidth(aDC, fnt, PAnsiChar(cur));
      until OutWidth^+tw<=MaxWidth;
    if ((MinLen.cx>0)and(OutWidth^+tw>MaxWidth)) then cur:= '';
    if (k<=0)or(cur='')or(k=nl) then Stop:= True;
    if cur<>'' then begin
      if minlen.cx=0 then l:= k-1;
      Delete(S, 1, l);
      tw:= GetTextWidth(aDC, fnt, PAnsiChar(cur));
      if k>0 then OutWidth^:=OutWidth^+tw;
      tw:= GetTextHeight(aDC, fnt, PAnsiChar(cur));
      if tw>OutHeight^ then OutHeight^:= tw;
    end;
    DeleteTags(S, tags);
    DeleteObject(fnt);
    if s<>'' then s:=tags+TrimEx(s);
  until (S='')or(Stop);
  ol:= Length(Origin);
  OutLen^:= ol-Length(S)+length(tags);
  if OutLen^>ol then OutLen^:= ol;
end;

procedure BuildText(Index: Pointer);
var
  gradbmp1, gradbmp2,
  clrbmp1, clrbmp2,
  shdbmp1, shdbmp2,
  tmpbmp1, tmpbmp2,
  tmpbmp3, tmpbmp4: HBITMAP;
  r1: TRect;
  s, tname, tval, tags, tmp, invtg: String;
  k, l, cWidth, cHeight: Integer;
  Space, aHeight, aWidth, aLeft, aTop: Integer;
  shdRes, MinLen: TSize;
  lfnt: TLogFont;
  OldCharExtra, RepeatCycles: Integer;
  TTL: TTextLog;
  fnt: HFONT;
Label
  NewImage;
begin
  with PShadowText(Index)^ do begin
    TTL.ImgWidth:= shdWidth+(shdShadowSize+TEXT_INDENTPIX)*2;
    TTL.ImgHeight:= shdHeight+(shdShadowSize+TEXT_INDENTPIX)*2;
    TTL.BitsCount:= (TTL.ImgWidth*TTL.ImgHeight);
    ClearBits(shdBits, TTL.BitsCount);
    if (shdCaption=nil)or(String(shdCaption)='') then goto NewImage;
    aLeft:= TEXT_INDENTANIS+(shdShadowSize*20); aTop:= TEXT_INDENTANIS+(shdShadowSize*20); minlen.cx:= 0; OldCharExtra:= 0;
    SetMapMode(shdTextDC, MM_ANISOTROPIC);
    SetWindowExtEx(shdTextDC, ISSpriteMain.DisplaySize.cx*20, ISSpriteMain.DisplaySize.cy*20, nil);
    SetViewportExtEx(shdTextDC, ISSpriteMain.DisplaySize.cx, ISSpriteMain.DisplaySize.cy, nil);
    s:= shdCaption; Space:= GetTextWidth(shdTextDC, shdFont, CHR_SPACE); RepeatCycles:= 0;
    TTL.TextDC:= shdTextDC;
    TTL.TextBits:= shdBits;
    TTL.CharExtra:= shdCharExtra;
    TTL.ShadowColor:= shdShadowColor;
    TTL.ShadowSize:= shdShadowSize;
    TTL.ShadowIntense:= shdShadowIntense;
    TTL.ShadowStyle:= shdFlags;

    CreateNewSurface(shdTextDC, TTL.ColorDC, TTL.ColorBits, clrbmp1, clrbmp2, @TTL, @ISSpriteMain.DisplaySize);
    CreateNewSurface(shdTextDC, TTL.ShadowDC, TTL.ShadowBits, shdbmp1, shdbmp2, @TTL, @ISSpriteMain.DisplaySize);
    CreateNewSurface(shdTextDC, TTL.TempDC, TTL.TempBits, tmpbmp1, tmpbmp2, @TTL, @ISSpriteMain.DisplaySize);
    CreateNewSurface(shdTextDC, TTL.TempDC2, TTL.TempBits2, tmpbmp3, tmpbmp4, @TTL, @ISSpriteMain.DisplaySize);
    if shdNeedGradient then
      CreateNewSurface(shdTextDC, TTL.GradientDC, TTL.GradientBits, gradbmp1, gradbmp2, @TTL, @ISSpriteMain.DisplaySize);

    shdRes.cx:= shdWidth; shdRes.cy:= shdHeight; DPToLP(shdTextDC, shdRes, 1);
    SetTextColor(TTL.TempDC, $FFFFFF);
    if (shdCharExtra<>0) then
      SetTextCharacterExtra(TTL.TempDC, GetTextCharacterExtra(TTL.TempDC)+shdCharExtra);

    repeat
      GetTextWidthEx(TTL.TempDC, S, shdFont, shdRes.cx, @l, @cWidth, @cHeight);
      tmp:= TrimEx(Cut(S, 1, l));
      if (S<>'')and(Pos(CHR_ENDLC, S)<>1)and(Pos(CHR_ENDLH, S)<>1) then S:=CHR_ENDLC+S;
      k:= Pos(TG_ALIGN, AnsiLowercase(tmp));
      if k>0 then begin
        GetTag(k, tmp, tname, tval, tags);
        tmp:= tags+tmp;
      end else
        tval:= AL_LEFT;
      case AnsiIndexText(AnsiLowercase(tval), [AL_LEFT, AL_CENTER, AL_RIGHT]) of
        0: aLeft:= TEXT_INDENTANIS + (shdShadowSize*20);
        1: aLeft:= TEXT_INDENTANIS + (shdShadowSize*20) + (shdRes.cx div 2) - (cWidth div 2);
        2: aLeft:= TEXT_INDENTANIS + (shdShadowSize*20) + shdRes.cx - cWidth;
      end;

      if tmp='' then Inc(RepeatCycles) else RepeatCycles:= 0;
      if (RepeatCycles = MAX_REPEAT) then Break;

      if tmp<>'' then repeat
        k:= Pos(CHR_LROUND, tmp); TTL.TextColor:= shdTextColor; tags:= ''; invtg:= ''; TTL.NeedGradient:= False;
        FillChar(lfnt, SizeOf(TLogFont), 0); GetObject(shdFont, SizeOf(TLogFont), @lfnt);
        if k=1 then
          repeat
            if (tmp='')or(tmp[1]<>CHR_LROUND)or((tmp[1]=CHR_LROUND)and(tmp[2]=CHR_SLASH)) then Break;
            l:= GetTag(1, tmp, tname, tval, tags);
            if tname=TAG_TAB then begin
              if tval<>'' then
                aLeft:= aLeft+StrToInt(tval)*Space
              else
                aLeft:= aLeft+5*Space;
              Delete(tags, Pos(TAG_TAB, tags)-1, l);
            end else
            if not ExpandTag(tname, tval, @lfnt, @MinLen.cx, @TTL.TextColor) then
              invtg:=invtg+cut(tags, pos(tname, tags)-1, l);
          until k=0;
        tmp:= invtg+tmp;
        if tmp='' then Break;
        k:= ForwardTag(tmp);
        if TTL.TextColor=shdTextColor then TTL.NeedGradient:= shdNeedGradient;
        TTL.TextFont:= CreateFontIndirect(lfnt);
        aHeight:= GetTextHeight(TTL.TempDC, TTL.TextFont, PAnsiChar(tmp));
        if cHeight=0 then cHeight:= aHeight;
        TTL.Text:= PAnsiChar(Cut(tmp, 1, k-1));
        DeleteTags(tmp, Tags);
        if (tmp<>'')and(tags<>'') then tmp:=tags+tmp;
        if TTL.Text<>'' then begin
          DPToLP(TTL.TempDC, MinLen, 1);
          if (MinLen.cx>0) then TTL.Text:= PAnsiChar(MinimizePathName(TTL.TempDC, TTL.Text, TTL.TextFont, minlen.cx));
          aWidth:= GetTextWidth(TTL.TempDC, TTL.TextFont, TTL.Text);
          TTL.TextLength:= Length(TTL.Text);
          SetRect(TTL.TextRect, aLeft, aTop+cHeight-aHeight, aWidth+aLeft, aTop+cHeight);
          fnt:= SelectObject(TTL.TempDC, TTL.TextFont);
          DrawShadowText(@TTL);
          SelectObject(TTL.TempDC, fnt);
          aLeft:= aLeft+aWidth{+Space};
        end;
        DeleteObject(TTL.TextFont);
      until tmp='';
      aLeft:= TEXT_INDENTANIS+(shdShadowSize*20);
      while (Pos(CHR_ENDLC, S)=1)or(Pos(CHR_ENDLH, S)=1) do begin
        Delete(S, 1, 2);
        aTop:= aTop+cHeight;
      end;
      if (s<>'')and(tags<>'') then S:=tags+TrimEx(S);
    until S='';

    SetRectIII(@r1, TTL.ImgWidth, TTL.ImgHeight);
    DPToLP(shdTextDC, r1, 2);

    if shdShadowSize<>0 then begin
      Shadowing(@TTL);
      SmoothBits(@TTL, shdBits);
    end;
    BlendBits(TTL.ColorBits, shdBits, TTL.BitsCount);

    if shdNeedGradient then begin
      SetRectIII(@TTL.TextRect, TTL.ImgWidth, TTL.ImgHeight);
      SetGradient(TTL.GradientBits, TTL.ImgWidth, TTL.ImgHeight, shdBeginColor, shdEndColor, shdFlags);
      BlendBits(TTL.GradientBits, shdBits, TTL.BitsCount);
      SelectObject(TTL.GradientDC, gradbmp2);
      DeleteObject(TTL.GradientDC);
      DeleteObject(gradbmp1);
    end;
    SetMapMode(shdTextDC, MM_TEXT);
    SelectObject(TTL.TempDC, tmpbmp2);
    DeleteObject(TTL.TempDC);
    DeleteObject(tmpbmp1);
    SelectObject(TTL.TempDC2, tmpbmp4);
    DeleteObject(TTL.TempDC2);
    DeleteObject(tmpbmp3);
    SelectObject(TTL.ColorDC, clrbmp2);
    DeleteObject(TTL.ColorDC);
    DeleteObject(clrbmp1);
    SelectObject(TTL.ShadowDC, shdbmp2);
    DeleteObject(TTL.ShadowDC);
    DeleteObject(shdbmp1);

NewImage:
    gdipDisposeImage(shdImage);
    GdipCreateBitmapFromScan0(TTL.ImgWidth, TTL.ImgHeight, TTL.ImgWidth shl 2, PixelFormat32bppPARGB, shdBits, shdImage);
  end;
end;

{$ifdef RELEASE}
function sp601(lpParent: HWND; lpLeft, lpTop, lpWidth, lpHeight: Smallint; lpText: PAnsiChar; lpTextColor: Integer; lpFont: HFONT): Longword; stdcall;
{$else}
function spShdCreate(lpParent: HWND; lpLeft, lpTop, lpWidth, lpHeight: Smallint; lpText: PAnsiChar; lpTextColor: Integer; lpFont: HFONT): Longword; stdcall;
{$endif}
var
  aDC     : HDC;
  FI      : PFormInfo;
  bmiInfo : TBitmapInfo;
  lfnt    : TLogFont;
  ST      : PShadowText;
begin
  Result:= INVALID_HANDLE_VALUE;

  FI:= spGetFormInfo(lpParent);
  if FI = nil then FI:= spAddFormInfo(lpParent);
  if (FI = nil) or (FI^.lpTextsCount = 0) then Exit;

  Dec(FI^.lpTextsCount);

  ST:= PShadowText(Longword(FI^.lpTexts) + Longword(FI^.lpTextsCount*SizeOf(TShadowText)));
  FillChar(ST^, SizeOf(TShadowText), 0);
  with ST^ do begin
    aDC               := GetDC(lpParent);
    shdTextDC         := CreateCompatibleDC(aDC);
    FillBmiInfo(lpWidth+(shdShadowSize+TEXT_INDENTPIX)*2, lpHeight+(shdShadowSize+TEXT_INDENTPIX)*2, @bmiInfo);
    shdTextBitmap     := CreateDIBSection(shdTextDC, bmiInfo, DIB_RGB_COLORS, shdBits, 0, 0);
    shdOldBitmap      := SelectObject(shdTextDC, shdTextBitmap);
    shdParent         := lpParent;
    ShdCaption:= HeapAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, Length(lpText)+1);
    strcopy(shdCaption, lpText, strlen(lpText));
    shdLeft           := lpLeft;
    shdTop            := lpTop;
    shdHeight         := lpHeight;
    shdWidth          := lpWidth;
    shdVisible        := True;
    shdShadowIntense  := 0;
    shdShadowSize     := 0;
    shdShadowColor    := 0;
    shdTextColor      := lpTextColor;
    shdFlags          := SHD_NEEDREPAINT;
    shdCharExtra      := 0;
    shdImage          := nil;

    Move((@shdLeft)^, (@shdOldLeft)^, 16);

    SetBkMode(shdTextDC, TRANSPARENT);
    FillChar(lfnt, SizeOf(TLogFont), 0);
    GetObject(lpFont, SizeOf(TLogFont), @lfnt);
    lfnt.lfHeight     := lfnt.lfHeight*20;
    shdFont           := CreateFontIndirect(lfnt);

    ReleaseDC(lpParent, aDC);

    shdCbSpace        := VirtualAlloc(nil, 6*$100, MEM_RESERVE or MEM_COMMIT, PAGE_EXECUTE_READWRITE);

    Result:= Longword(ST);
  end;
end;

{$ifdef RELEASE}
procedure sp602(hText: HBITMAP; lpBeginColor, lpEndColor: Cardinal; lpStyle: DWORD); stdcall;
{$else}
procedure spShdAddGradient(hText: HBITMAP; lpBeginColor, lpEndColor: Cardinal; lpStyle: DWORD); stdcall;
{$endif}
var
	ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    shdNeedGradient:= True;
    shdBeginColor:= lpBeginColor;
    shdEndColor:= lpEndColor;
    shdFlags:= shdFlags or lpStyle or SHD_NEEDREPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp603(hText: HBITMAP); stdcall;
{$else}
procedure spShdFreeGradient(hText: HBITMAP); stdcall;
{$endif}
var
  ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    shdNeedGradient:= False;
    shdBeginColor:= 0;
    shdEndColor:= 0;
    shdFlags:= shdFlags or SHD_NEEDREPAINT and not (SHD_GRADIENTVER or SHD_GRADIENTHOR);
  end;
end;

{$ifdef RELEASE}
function sp604(hText: HBITMAP): HWND; stdcall;
{$else}
function spShdGetParent(hText: HBITMAP): HWND; stdcall;
{$endif}
var
  ST: PShadowText;
begin
  Result:= INVALID_HANDLE_VALUE;
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  Result:= ST^.shdParent;
end;

{$ifdef RELEASE}
procedure sp605(hText: HBITMAP; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spShdGetPosition(hText: HBITMAP; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
  ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    lpLeft:= shdLeft;
    lpTop:= shdTop;
    lpHeight:= shdHeight;
    lpWidth:= shdWidth;
  end;
end;

{$ifdef RELEASE}
function sp606(hText: HBITMAP): Integer; stdcall;
{$else}
function spShdGetRotateAngle(hText: HBITMAP): Integer; stdcall;
{$endif}
var
  ST: PShadowText;
begin
  Result:= 0;
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  Result:= ST^.shdRotate;
end;

{$ifdef RELEASE}
function sp607(hText: HBITMAP): Longword; stdcall;
{$else}
function spShdGetShadowColor(hText: HBITMAP): Longword; stdcall;
{$endif}
var
  ST: PShadowText;
begin
  Result:= INVALID_HANDLE_VALUE;
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  Result:= ST^.shdShadowColor;
end;

{$ifdef RELEASE}
procedure sp608(hText: HBITMAP; var lpText: PAnsiChar; iBuffSize: Longint); stdcall;
{$else}
procedure spShdGetTag(hText: HBITMAP; var lpText: PAnsiChar; iBuffSize: Longint); stdcall;
{$endif}
var
  ST: PShadowText;
  l: Longint;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    if shdTag = nil then Exit;

    l:= strlen(shdTag) + 1;
    if (l > iBuffSize) then l:= iBuffSize;

    strcopy(lpText, shdTag, l);
  end;
end;

{$ifdef RELEASE}
function sp609(hText: HBITMAP): Longword; stdcall;
{$else}
function spShdGetTextColor(hText: HBITMAP): Longword; stdcall;
{$endif}
var
  ST: PShadowText;
begin
  Result:= INVALID_HANDLE_VALUE;
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  Result:= ST^.shdTextColor;
end;

{$ifdef RELEASE}
function sp610(hText: HBITMAP): Boolean; stdcall;
{$else}
function spShdGetVisible(hText: HBITMAP): Boolean; stdcall;
{$endif}
var
  ST: PShadowText;
begin
  Result:= False;
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  Result:= ST^.shdVisible;
end;

{$ifdef RELEASE}
procedure sp611(hText: HBITMAP; lpCharExtra: Integer); stdcall;
{$else}
procedure spShdSetCharacterExtra(hText: HBITMAP; lpCharExtra: Integer); stdcall;
{$endif}
var
  ST: PShadowText;
begin
	ST:= spIsValidText(hText);
  if ST = nil then Exit;

	with ST^ do begin
		shdCharExtra:= lpCharExtra;
		shdFlags:= shdFlags or SHD_NEEDREPAINT;
	end;
end;

{$ifdef RELEASE}
procedure sp612(hText: HBITMAP; lpEventID: Integer; lpEvent, lpID: DWORD); stdcall;
{$else}
procedure spShdSetEvent(hText: HBITMAP; lpEventID: Integer; lpEvent, lpID: DWORD); stdcall;
{$endif}
var
  ST: PShadowText;
  cur: Pointer;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    cur:= Pointer(Longint(shdCbSpace)+lpEventID*$100);
    Move((@MouseCmnCallback)^, cur^, 64);
    PDWORD(Longword(cur)+5)^:= lpID;
    PDWORD(Longword(cur)+10)^:= lpEvent;
    case lpEventID of
      cmnMouseClickEvent: shdMouseClick:= cur;
      cmnMouseEnterEvent: shdMouseEnter:= cur;
      cmnMouseLeaveEvent: shdMouseLeave:= cur;
      cmnMouseMoveEvent : shdMouseMove := cur;
      cmnMouseDownEvent : shdMouseDown := cur;
      cmnMouseUpEvent   : shdMouseUp   := cur;
    end;
  end;
end;

{$ifdef RELEASE}
procedure sp613(hText: HBITMAP; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spShdSetPosition(hText: HBITMAP; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
  ST: PShadowText;
  bmiInfo: TBitmapInfo;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    if (shdLeft = lpLeft) and (shdTop = lpTop) and
       (shdWidth = lpWidth) and (shdHeight = lpHeight) then Exit;
    Move((@shdLeft)^, (@shdOldLeft)^, 16);

    shdLeft:= lpLeft;
    shdTop:= lpTop;
    shdHeight:= lpHeight;
    shdWidth:= lpWidth;

    shdFlags:= shdFlags or SHD_NEEDPAINT;

    if (shdOldWidth <> lpWidth) or (shdOldHeight <> lpHeight) then begin
      SelectObject(shdTextDC, shdOldBitmap);
      DeleteObject(shdTextBitmap);
      FillBmiInfo(shdWidth+(shdShadowSize+TEXT_INDENTPIX)*2, shdHeight+(shdShadowSize+TEXT_INDENTPIX)*2, @bmiInfo);
      shdTextBitmap:= CreateDIBSection(shdTextDC, bmiInfo, DIB_RGB_COLORS, shdBits, 0, 0);
      shdOldBitmap:= SelectObject(shdTextDC, shdTextBitmap);

      shdFlags:= shdFlags or SHD_NEEDBUILD;
    end;
  end;
end;

{$ifdef RELEASE}
procedure sp614(hText: HBITMAP; lpAngle: Integer); stdcall;
{$else}
procedure spShdSetRotateAngle(hText: HBITMAP; lpAngle: Integer); stdcall;
{$endif}
var
  ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    shdRotate:= lpAngle;
    shdFlags:= shdFlags or SHD_NEEDPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp615(hText: HDC; lpShadowColor: Cardinal; lpShadowSize, lpShadowIntense: Integer; lpShadowStyle: DWORD); stdcall;
{$else}
procedure spShdSetShadow(hText: HDC; lpShadowColor: Cardinal; lpShadowSize, lpShadowIntense: Byte; lpShadowStyle: DWORD); stdcall;
{$endif}
var
  ST: PShadowText;
  bmiInfo: TBitmapInfo;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    shdShadowColor:= lpShadowColor;
    shdShadowSize:= lpShadowSize;
    shdShadowIntense:= lpShadowIntense;

    if shdShadowSize <> 0 then begin
      SelectObject(shdTextDC, shdOldBitmap);
      DeleteObject(shdTextBitmap);
      FillBmiInfo(shdWidth+(shdShadowSize+TEXT_INDENTPIX)*2, shdHeight+(shdShadowSize+TEXT_INDENTPIX)*2, @bmiInfo);
      shdTextBitmap:= CreateDIBSection(shdTextDC, bmiInfo, DIB_RGB_COLORS, shdBits, 0, 0);
      shdOldBitmap:= SelectObject(shdTextDC, shdTextBitmap);
    end;

    shdFlags:= shdFlags and (not (SHD_STYLEGLOW or SHD_STYLESHADOW)) or lpShadowStyle or SHD_NEEDPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp616(hText: HBITMAP; lpShadowColor: Cardinal); stdcall;
{$else}
procedure spShdSetShadowColor(hText: HBITMAP; lpShadowColor: Cardinal); stdcall;
{$endif}
var
  ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    if shdShadowSize <> 0 then begin
      shdShadowColor:= lpShadowColor;
      shdFlags:= shdFlags or SHD_NEEDREPAINT;
    end;
  end;
end;

{$ifdef RELEASE}
procedure sp617(hText: HBITMAP; lpText: PAnsiChar); stdcall;
{$else}
procedure spShdSetTag(hText: HBITMAP; lpText: PAnsiChar); stdcall;
{$endif}
var
  ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    if shdTag <> nil then shdTag:= HeapReAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, shdTag, strlen(lpText)+1)
    else shdTag:= HeapAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, strlen(lpText)+1);

    strcopy(shdTag, lpText, strlen(lpText));
  end;
end;

{$ifdef RELEASE}
procedure sp618(hText: HBITMAP; lpCaption: PAnsiChar); stdcall;
{$else}
procedure spShdSetText(hText: HBITMAP; lpCaption: PAnsiChar); stdcall;
{$endif}
var
  ST: PShadowText;
  l1, l2: Integer;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    l1:= strlen(shdCaption);
    l2:= strlen(lpCaption);
    if ((CompareString(LOCALE_USER_DEFAULT, NORM_IGNORECASE, shdCaption, l1, lpCaption, l2) - 2)=0) then Exit;
    FillChar(shdCaption^, l1, 0);
    if l2 <> l1 then
      shdCaption:= HeapReAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, shdCaption, l2+1);
    //StrLCopy(shdCaption, lpCaption, l2);
    strcopy(shdCaption, lpCaption, l2);
    shdFlags:= shdFlags or SHD_NEEDREPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp619(hText: HBITMAP; lpTextColor: Cardinal); stdcall;
{$else}
procedure spShdSetTextColor(hText: HBITMAP; lpTextColor: Cardinal); stdcall;
{$endif}
var
  ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    shdTextColor:= lpTextColor;
    shdFlags:= shdFlags or SHD_NEEDREPAINT;
  end;
end;

{$ifdef RELEASE}
procedure sp620(hText: HBITMAP; lpVisible: Boolean); stdcall;
{$else}
procedure spShdSetVisible(hText: HBITMAP; lpVisible: Boolean); stdcall;
{$endif}
var
  ST: PShadowText;
begin
  ST:= spIsValidText(hText);
  if ST = nil then Exit;

  with ST^ do begin
    shdVisible:= lpVisible;
    shdFlags:= shdFlags or SHD_NEEDPAINT;
  end;
end;

exports
{$ifdef RELEASE}
	sp601,
	{gradient}
	sp602, sp603,
	{get}
	sp604, sp605, sp606, sp607, sp608, sp609, sp610,
	{set}
	sp611, sp612, sp613, sp614, sp615, sp616, sp617, sp618, sp619, sp620;
{$else}
	spShdCreate,
  {gradient}
	spShdAddGradient,
	spShdFreeGradient,
  {get}
	spShdGetParent,
	spShdGetPosition,
  spShdGetRotateAngle,
	spShdGetShadowColor,
	spShdGetTag,
	spShdGetTextColor,
	spShdGetVisible,
  {set}
	spShdSetCharacterExtra,
	spShdSetEvent,
	spShdSetPosition,
	spShdSetRotateAngle,
	spShdSetShadow,
	spShdSetShadowColor,
	spShdSetTag,
	spShdSetText,
	spShdSetTextColor,
	spShdSetVisible;
{$endif}