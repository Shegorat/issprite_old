function spBtnAddButton(hParent: HWND; lpBitmap: GPBITMAP; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
var
  FI			  : PFormInfo;
  lpImgBtn  : PImgButton;
  x, y		  : Cardinal;
  z			 		: Smallint;
  tmpDC		  : HDC;
  bmiInfo	  : TBitmapInfo;
begin
  Result:= INVALID_HANDLE_VALUE;

  FI:= spGetFormInfo(hParent);
  if FI = nil then FI:= spAddFormInfo(hParent);

  if (FI = nil) or (FI^.lpButtonsCount = 0) then Exit;
  Dec(FI^.lpButtonsCount);

  lpImgBtn:= PImgButton(Longword(FI^.lpButtons) + Longword(FI^.lpButtonsCount*SizeOf(TImgButton)));
  FillChar(lpImgBtn^, SizeOf(TImgButton), 0);

  with lpImgBtn^ do begin
    btnParent         := hParent;
    btnParentFI       := FI;
    btnLeft           := lpLeft;
    btnTop            := lpTop;
    btnWidth          := lpWidth;
    btnHeight         := lpHeight;

    btnHandle         := CreateWindowEx(WS_EX_TRANSPARENT, btnClassName, nil, WS_CHILD or WS_VISIBLE, lpLeft, lpTop, lpWidth, lpHeight, hParent, 0, hInstance, nil);
    btnImage          := lpBitmap;

    GdipGetImageWidth(btnImage, x);
    GdipGetImageHeight(btnImage, y);

    case lpStyle of
      BTN_PANEL       : z:= 1;
      BTN_BUTTON      : z:= 4;
      BTN_CHECKBOX    : z:= 8;
      BTN_CHECKBOXEX  : z:= 12;
      BTN_TAB         : z:= 8;
      else begin
        z	            := 1;
        lpStyle       := BTN_PANEL; {my_add}
       end;
    end;

    btnImgWidth       := x;
    btnImgHeight      := y;
    btnImgStateH      := btnImgHeight div z;
    btnCurImageState  := 1;
    btnImgStateCount  := z;
    btnAspectRatio    := x/btnImgStateH;

    btnStyle          := lpStyle;
    btnShadowSize     := lpShadowSize;
    btnTransparent    := 255;

    case btnStyle of
      BTN_CHECKBOX, BTN_CHECKBOXEX: begin
        btnTextAlign  := DT_LEFT or DT_VCENTER or DT_SINGLELINE;
        btnAddWidth   := trunc(btnHeight*btnAspectRatio);
        btnTextILeft  := btnAddWidth+6;
      end;
      BTN_PANEL, BTN_BUTTON, BTN_TAB: begin
        btnAddWidth   := btnWidth;
        btnTextAlign  := DT_CENTER or DT_VCENTER or DT_SINGLELINE;
        btnTextILeft  := 0;
      end;
    end;
    
    btnTextITop       := 0;

    btnText           := nil;
    btnTag            := nil;

    btnCursor         := INVALID_HANDLE_VALUE;
    btnOldCursor      := INVALID_HANDLE_VALUE;

    btnRebuild        := True;
    btnEnabled        := True;
    btnVisible        := True;

    btnMouseMoved     := False;
    btnMousePressed   := False;

    tmpDC             := GetDC(btnHandle);
    btnCacheDC        := CreateCompatibleDC(tmpDC);
    FillBmiInfo(btnWidth, btnHeight*z, @bmiInfo);
    btnCacheBmp       := CreateDIBSection(btnCacheDC, bmiInfo, DIB_RGB_COLORS, btnCacheBits, 0, 0);
    btnCacheOld       := SelectObject(btnCacheDC, btnCacheBmp);

    btnSurface        := CreateCompatibleDC(tmpDC);
    FillBmiInfo(btnWidth, btnHeight, @bmiInfo);
    btnSurfaceBmp     := CreateDIBSection(btnSurface, bmiInfo, DIB_RGB_COLORS, btnSurfaceBits, 0, 0);
    btnSurfaceOld     := SelectObject(btnSurface, btnSurfaceBmp);

    btnBitsCount      := (btnWidth*btnHeight);

    ReleaseDC(btnHandle, tmpDC);

    btnCbSpace        := VirtualAlloc(nil, 6*$100, MEM_RESERVE or MEM_COMMIT, PAGE_EXECUTE_READWRITE);

    Result            := btnHandle;
  end;
end;

{$ifdef RELEASE}
function sp401(hParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$else}
function spBtnCreateButton(hParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$endif}
var
  TmpImg: GPBITMAP;
begin
  Result:= INVALID_HANDLE_VALUE;
  TmpImg:= GDILoadFromFile(lpFilename);
  if TmpImg <> nil then
    Result:= spBtnAddButton(hParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStyle, lpShadowSize);
end;

{$ifdef RELEASE}
function sp402(hParent: HWND; lpBuffer: Pointer; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$else}
function spBtnCreateButtonFromBuffer(hParent: HWND; lpBuffer: Pointer; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$endif}
var
  TmpImg: GPBITMAP;
begin
  Result:= INVALID_HANDLE_VALUE;
  TmpImg:= GDILoadFromBufer(lpBuffer, lpBuffSize);
  if TmpImg <> nil then
    Result:= spBtnAddButton(hParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStyle, lpShadowSize);
end;

{$ifdef RELEASE}
function sp403(hParent: HWND; lpInstance: Longword; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$else}
function spBtnCreateButtonFromResourceName(hParent: HWND; lpInstance: Longword; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$endif}
var
	hRes, hResLoad: Longword;
  ResSize: Cardinal;
  TmpImg: GPBITMAP;
  pRes: Pointer;
begin
  Result:= Longword(nil);

  if lpInstance <> 0 then begin
    hRes:= FindResource(lpInstance, lpResName, ResourceSwitch(lpResType));

    if hRes <> 0 then begin
      hResLoad:= LoadResource(lpInstance, hRes);
      if hResLoad <> 0 then begin
        ResSize:= SizeOfResource(lpInstance, hRes);
        pRes:= LockResource(hResLoad);

        TmpImg:= GDILoadFromBufer(pRes, ResSize);
        if TmpImg <> nil then
          Result:= spBtnAddButton(hParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStyle, lpShadowSize);
      end;
    end;
  end;
end;

{$ifdef RELEASE}
function sp404(hParent: HWND; lpResLibrary: PAnsiChar; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$else}
function spBtnCreateButtonFromResourceLibrary(hParent: HWND; lpResLibrary: PAnsiChar; lpResName: PAnsiChar; lpResType: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND; stdcall;
{$endif}
var
	hLib, hRes, hResLoad: Longword;
  ResSize: Cardinal;
  TmpImg: GPBITMAP;
  pRes: Pointer;
begin
  Result:= Longword(nil);

  hLib:= LoadLibraryA(lpResLibrary);

  if hLib <> 0 then begin
    hRes:= FindResourceA(hLib, lpResName, ResourceSwitch(lpResType));

    if hRes <> 0 then begin
      hResLoad:= LoadResource(hLib, hRes);

      if hResLoad <> 0 then begin
        ResSize:= SizeOfResource(hLib, hRes);
        pRes:= LockResource(hResLoad);

        TmpImg:= GDILoadFromBufer(pRes, ResSize);
        if TmpImg <> nil then
          Result:= spBtnAddButton(hParent, TmpImg, lpLeft, lpTop, lpWidth, lpHeight, lpStyle, lpShadowSize);
      end;
    end;
    FreeLibrary(hLib);
  end;
end;

{$ifdef RELEASE}
procedure sp405(hBtn: HWND); stdcall;
{$else}
procedure spBtnRefresh(hBtn: HWND); stdcall;
{$endif}
var
  IB: PImgButton;
  r: TRect;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    SetRectIII(@r, btnWidth, btnHeight);
    InvalidateRect(btnHandle, @r, False);
    UpdateWindow(btnHandle);
  end;
end;

{$ifdef RELEASE}
function sp406(hBtn: HWND): Byte; stdcall;
{$else}
function spBtnGetChecked(hBtn: HWND): Byte; stdcall;
{$endif}
var
  IB: PImgButton;
begin
  Result:= 0;
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  Result:= IB^.btnCheckState;
end;

{$ifdef RELEASE}
function sp407(hBtn: HWND): Boolean; stdcall;
{$else}
function spBtnGetEnabled(hBtn: HWND): Boolean; stdcall;
{$endif}
var
  IB: PImgButton;
begin
  Result:= False;
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  Result:= IB^.btnEnabled;
end;

{$ifdef RELEASE}
function sp408(hBtn: HWND): HWND; stdcall;
{$else}
function spBtnGetParent(hBtn: HWND): HWND; stdcall;
{$endif}
var
  IB: PImgButton;
begin
  Result:= INVALID_HANDLE_VALUE;
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  Result:= IB^.btnParent;
end;

{$ifdef RELEASE}
procedure sp409(hBtn: HWND; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spBtnGetPosition(hBtn: HWND; var lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    lpLeft   := btnLeft;
    lpTop    := btnTop;
    lpWidth  := btnWidth;
    lpHeight := btnHeight;
  end;
end;

{$ifdef RELEASE}
function sp410(CursorID: Integer): HCURSOR; stdcall;
{$else}
function spBtnGetSysCursor(CursorID: Integer): HCURSOR; stdcall;
{$endif}
var
	cur: Integer;
begin
  case CursorID of
      0  {---}: cur:= 32512; {crDefault}
		  1, 32649: cur:= 32649; {crHand}
		 -1  {---}: cur:= 00000; {crNone}
	   -2, 32512: cur:= 32512; {crArrow}
	   -3, 32515: cur:= 32515; {crCross} 
	   -4, 32513: cur:= 32513; {crIBeam}
     {-} 32640: cur:= 32640; {IDC_SIZE}
     {-} 32641: cur:= 32641; {IDC_ICON}
	   -6, 32643: cur:= 32643; {crSizeNESW}
	   -7, 32645: cur:= 32645; {crSizeNS}
	   -8, 32642: cur:= 32642; {crSizeNWSE}
	   -9, 32644: cur:= 32644; {crSizeWE} 
	  -10, 32516: cur:= 32516; {crUpArrow}
	  -11, 32514: cur:= 32514; {crHourGlass}
	  -18, 32648: cur:= 32648; {crNo} 
	  -19, 32650: cur:= 32650; {crAppStart}
	  -20, 32651: cur:= 32651; {crHelp} 
	  -22, 32646: cur:= 32646; {crSizeAll} 
		else cur:= 32512;
	end;
	Result:= LoadCursorA(0, PAnsiChar(cur));
end;

{$ifdef RELEASE}
procedure sp411(hBtn: HWND; var lpText: PAnsiChar; lpBuffSize: Integer); stdcall;
{$else}
procedure spBtnGetTag(hBtn: HWND; var lpText: PAnsiChar; lpBuffSize: Integer); stdcall;
{$endif}
var
  IB: PImgButton;
  l: Longint;
begin
  IB:= spGetButton(hBtn);
  if (IB = nil) or (lpText = nil) then Exit;

  with IB^ do begin
    l:= strlen(btnTag) + 1;
    if (l > lpBuffSize) then l:= lpBuffSize;

    strcopy(lpText, btnTag, l);
  end;
end;

{$ifdef RELEASE}
function sp412(hBtn: HWND): Byte; stdcall;
{$else}
function spBtnGetTransparent(hBtn: HWND): Byte; stdcall;
{$endif}
var
  IB: PImgButton;
begin
  Result:= 0;
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  Result:= IB^.btnTransparent;
end;

{$ifdef RELEASE}
function sp413(hBtn: HWND): Boolean; stdcall;
{$else}
function spBtnGetVisible(hBtn: HWND): Boolean; stdcall;
{$endif}
var
  IB: PImgButton;
begin
  Result:= False;
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  Result:= IB^.btnVisible;
end;

{$ifdef RELEASE}
procedure sp414(hBtn: HWND; lpChecked: Byte); stdcall;
{$else}
procedure spBtnSetChecked(hBtn: HWND; lpChecked: Byte); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    if btnStyle > BTN_BUTTON then begin
      if (lpChecked > 1) and (btnStyle = BTN_CHECKBOX) then lpChecked:= 1
      else if (lpChecked > 2) and (btnStyle = BTN_CHECKBOX) then lpChecked:= 2;
			
      btnCheckState:= lpChecked;
			
			if btnEnabled then btnCurImageState:= (lpChecked*4) + 1
			else btnCurImageState:= (lpChecked*4) + 4; {my_add}
    end;
  end;
end;

{$ifdef RELEASE}
procedure sp415(hBtn: HWND; lpCursor: HCURSOR); stdcall;
{$else}
procedure spBtnSetCursor(hBtn: HWND; lpCursor: HCURSOR); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  IB^.btnCursor:= lpCursor;
end;

{$ifdef RELEASE}
procedure sp416(Btn: HWND; FileName: PAnsiChar); stdcall;
{$else}
procedure spBtnSetCursorFile(Btn: HWND; FileName: PAnsiChar); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(Btn);
  if (IB = nil) or (Length(FileName) = 0) then Exit;

  DeleteObject(IB^.btnCursor);
	IB^.btnCursor:= LoadCursorFromFile(PAnsiChar(FileName));
end;

{$ifdef RELEASE}
procedure sp417(hBtn: HWND; lpEnabled: Boolean); stdcall;
{$else}
procedure spBtnSetEnabled(hBtn: HWND; lpEnabled: Boolean); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    if (btnEnabled = lpEnabled) then Exit;
    EnableWindow(hBtn, lpEnabled);
  end;
end;

{$ifdef RELEASE}
procedure sp418(hBtn: HWND; lpEventID: Integer; lpEvent, lpID: DWORD); stdcall;
{$else}
procedure spBtnSetEvent(hBtn: HWND; lpEventID: Integer; lpEvent, lpID: DWORD); stdcall;
{$endif}
var
  IB: PImgButton;
  cur: Pointer;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    cur:= Pointer(Longint(btnCbSpace) + lpEventID*$100);
    Move((@MouseCmnCallback)^, cur^, 64);
    PDWORD(Longword(cur)+5)^:= lpID;
    PDWORD(Longword(cur)+10)^:= lpEvent;
    case lpEventID of
      cmnMouseClickEvent: btnMouseClick := cur;
      cmnMouseEnterEvent: btnMouseEnter := cur;
      cmnMouseLeaveEvent: btnMouseLeave := cur;
      cmnMouseMoveEvent : btnMouseMove  := cur;
      cmnMouseDownEvent : btnMouseDown  := cur;
      cmnMouseUpEvent   : btnMouseUp    := cur;
    end;
  end;
end;

{$ifdef RELEASE}
procedure sp419(hBtn: HWND; lpFont: HFONT); stdcall;
{$else}
procedure spBtnSetFont(hBtn: HWND; lpFont: HFONT); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  IB^.btnFont:= lpFont;
	IB^.btnRebuild:= True;
end;

{$ifdef RELEASE}
procedure sp420(hBtn: HWND; lpColorDefault, lpColorHover, lpColorPressed, lpColorDisabled: Cardinal); stdcall;
{$else}
procedure spBtnSetFontColor(hBtn: HWND; lpColorDefault, lpColorHover, lpColorPressed, lpColorDisabled: Cardinal); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    btnTextColorE := lpColorDefault;
    btnTextColorH := lpColorHover;
    btnTextColorP := lpColorPressed;
    btnTextColorD := lpColorDisabled;
		
    btnRebuild    := True;
  end;
end;

{$ifdef RELEASE}
procedure sp421(hBtn: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$else}
procedure spBtnSetPosition(hBtn: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer); stdcall;
{$endif}
var
  IB: PImgButton;
  bmiInfo: TBitmapInfo;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    if (btnLeft = lpLeft) and (btnTop = lpTop) and
       (btnWidth = lpWidth )and (btnHeight = lpHeight) then Exit;

    if (btnWidth <> lpWidth) or (btnHeight <> lpHeight) and (lpHeight > 0)and(lpWidth > 0) then begin
      btnRebuild    := True;
      btnBitsCount  := lpWidth*lpHeight;

      FillBmiInfo(lpWidth, lpHeight*btnImgStateCount, @bmiInfo);
      SelectObject(btnCacheDC, btnCacheOld);
      DeleteObject(btnCacheBmp);
      btnCacheBmp:= CreateDIBSection(btnCacheDC, bmiInfo, DIB_RGB_COLORS, btnCacheBits, 0, 0);
      btnCacheOld:= SelectObject(btnCacheDC, btnCacheBmp);

      FillBmiInfo(lpWidth, lpHeight, @bmiInfo);
      SelectObject(btnSurface, btnSurfaceOld);
      DeleteObject(btnSurfaceBmp);
      btnSurfaceBmp:= CreateDIBSection(btnSurface, bmiInfo, DIB_RGB_COLORS, btnSurfaceBits, 0, 0);
      btnSurfaceOld:= SelectObject(btnSurface, btnSurfaceBmp);

      if btnImgStateCount <= 4 then
        btnAddWidth:= lpWidth
      else begin
        btnAddWidth:= Trunc(lpHeight*btnAspectRatio);
        if btnStyle <> BTN_TAB then btnTextILeft:= btnAddWidth + 6
				else btnAddWidth:= lpWidth; {my_add}
      end;
    end;

    btnLeft   := lpLeft;
    btnTop    := lpTop;
    btnWidth  := lpWidth;
    btnHeight := lpHeight;

    MoveWindow(hBtn, lpLeft, lpTop, lpWidth, lpHeight, True);
  end;
end;

{$ifdef RELEASE}
procedure sp422(hBtn: HWND; lpText: PAnsiChar); stdcall;
{$else}
procedure spBtnSetTag(hBtn: HWND; lpText: PAnsiChar); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    if btnTag <> nil then
      btnTag:= HeapReAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, btnTag, strlen(lpText) + 1)
    else
      btnTag:= HeapAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, strlen(lpText) + 1);

    strcopy(btnTag, lpText, strlen(lpText));
  end;
end;

{$ifdef RELEASE}
procedure sp423(hBtn: HWND; lpText: PAnsiChar); stdcall;
{$else}
procedure spBtnSetText(hBtn: HWND; lpText: PAnsiChar); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    if (btnText <> nil) then
      btnText:= HeapReAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, btnText, strlen(lpText) + 1)
    else
      btnText:= HeapAlloc(ISSpriteMain.hProcHeap, HEAP_ZERO_MEMORY, strlen(lpText) + 1);

    strcopy(btnText, lpText, strlen(lpText));
    btnRebuild:= True;
  end;
end;

{$ifdef RELEASE}
procedure sp424(hBtn: HWND; lpAlign: Byte); stdcall;
{$else}
procedure spBtnSetTextAlignment(hBtn: HWND; lpAlign: Byte); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;
		
  case lpAlign of
    1: begin
			if IB^.btnImgStateCount <= 4 then
				IB^.btnTextAlign:= DT_CENTER or DT_VCENTER or DT_SINGLELINE
			else
				if IB^.btnStyle = BTN_TAB then IB^.btnTextAlign:= DT_CENTER or DT_VCENTER or DT_SINGLELINE {my_add}
				else IB^.btnTextAlign:= DT_LEFT or DT_VCENTER or DT_SINGLELINE;
        if (IB^.btnStyle = BTN_CHECKBOX) or (IB^.btnStyle = BTN_CHECKBOXEX) then IB^.btnTextILeft:= IB^.btnAddWidth + 6; {my_add}  
      end;
    2: begin
      IB^.btnTextAlign:= DT_TOP or DT_LEFT;
      if (IB^.btnStyle = BTN_CHECKBOX) or (IB^.btnStyle = BTN_CHECKBOXEX) then IB^.btnTextILeft:= IB^.btnAddWidth + 6; {my_add} 
     end;
    3: begin
      IB^.btnTextAlign:= DT_BOTTOM or DT_LEFT or DT_SINGLELINE;
      if (IB^.btnStyle = BTN_CHECKBOX) or (IB^.btnStyle = BTN_CHECKBOXEX) then IB^.btnTextILeft:= IB^.btnAddWidth + 6; {my_add} 
    end;
    4: IB^.btnTextAlign:= DT_TOP or DT_RIGHT;
    5: IB^.btnTextAlign:= DT_BOTTOM or DT_RIGHT or DT_SINGLELINE;
    6: IB^.btnTextAlign:= DT_TOP or DT_CENTER;
    7: IB^.btnTextAlign:= DT_BOTTOM or DT_CENTER or DT_SINGLELINE;
    8: IB^.btnTextAlign:= DT_VCENTER or DT_CENTER or DT_SINGLELINE;
  end;
	IB^.btnRebuild:= True;
end;

{$ifdef RELEASE}
procedure sp425(hBtn: HWND; iLeft, iTop: Integer); stdcall;
{$else}
procedure spBtnSetTextIndent(hBtn: HWND; iLeft, iTop: Integer); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  if (iLeft = -1) and (iTop = -1) then begin
    if IB^.btnStyle >= BTN_CHECKBOX then IB^.btnTextILeft:= IB^.btnAddWidth + 6
    else IB^.btnTextILeft:= 0;
		IB^.btnTextITop:= 0;
  end else begin
    IB^.btnTextITop:= iTop;
    IB^.btnTextILeft:= iLeft;
  end;
	IB^.btnRebuild:= True;
end;

{$ifdef RELEASE}
procedure sp426(hBtn: HWND; lpAlpha: Byte); stdcall;
{$else}
procedure spBtnSetTransparent(hBtn: HWND; lpAlpha: Byte); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  IB^.btnTransparent:= lpAlpha;
end;

{$ifdef RELEASE}
procedure sp427(hBtn: HWND; lpVisible: Boolean); stdcall;
{$else}
procedure spBtnSetVisible(hBtn: HWND; lpVisible: Boolean); stdcall;
{$endif}
var
  IB: PImgButton;
begin
  IB:= spGetButton(hBtn);
  if IB = nil then Exit;

  with IB^ do begin
    if (btnVisible = lpVisible) then Exit;

    btnVisible:= lpVisible;
    if lpVisible then ShowWindow(hBtn, SW_SHOW)
    else ShowWindow(hBtn, SW_HIDE);
  end;
end;

exports
{$ifdef RELEASE}
	sp401, sp402, sp403, sp404, sp405,
	{get}
	sp406, sp407, sp408, sp409,
  sp410, sp411, sp412, sp413,
	{set}
	sp414, sp415, sp416, sp417, sp418, sp419, sp420,
  sp421, sp422, sp423, sp424, sp425, sp426, sp427;
{$else}
	spBtnCreateButton,
	spBtnCreateButtonFromBuffer,
	spBtnCreateButtonFromResourceName,
	spBtnCreateButtonFromResourceLibrary,
	spBtnRefresh,
	{get}
	spBtnGetChecked,
  spBtnGetSysCursor,
	spBtnGetEnabled,
	spBtnGetParent,
	spBtnGetPosition,
	spBtnGetTag,
	spBtnGetTransparent,
	spBtnGetVisible,
	{set}
	spBtnSetChecked,
	spBtnSetCursor,
  spBtnSetCursorFile,
	spBtnSetEnabled,
	spBtnSetEvent,
	spBtnSetFont,
	spBtnSetFontColor,
	spBtnSetPosition,
	spBtnSetTag,
	spBtnSetText,
	spBtnSetTextAlignment,
	spBtnSetTextIndent,
	spBtnSetTransparent,
	spBtnSetVisible;
{$endif}