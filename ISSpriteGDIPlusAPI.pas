type
  Unit_ = (
    UnitWorld,
    UnitDisplay,
    UnitPixel,
    UnitPoint,
    UnitInch,
    UnitDocument,
    UnitMillimeter
  );
  TUnit = Unit_;

  GdiplusStartupInput = packed record
    GdiplusVersion          : Cardinal;
    DebugEventCallback      : Pointer;
    SuppressBackgroundThread: BOOL;
    SuppressExternalCodecs  : BOOL;          
  end;
  TGdiplusStartupInput = GdiplusStartupInput;
  PGdiplusStartupInput = ^TGdiplusStartupInput;

  TARGB = packed record
    rgbBlue: Byte;
    rgbGreen: Byte;
    rgbRed: Byte;
    rgbAlpha: Byte;
  end;
  PARGB = ^TARGB;
    
  PGPRect = ^TGPRect;
  TGPRect = packed record
    X     : Integer;
    Y     : Integer;
    Width : Integer;
    Height: Integer;
  end;

  PGPPoint = ^TGPPoint;
  TGPPoint = packed record
    X : Integer;
    Y : Integer;
  end;
{
  ImageLockMode = Integer;
  TImageLockMode = ImageLockMode;
}
  PixelFormat = Integer;
  TPIXELFORMAT = PixelFormat;
{
  BitmapData = packed record
    Width       : UINT;
    Height      : UINT;
    Stride      : Integer;
    PixelFormat : PixelFormat;
    Scan0       : Pointer;
    Reserved    : UINT;
  end;
  TBitmapData = BitmapData;
  PBitmapData = ^TBitmapData;
}
  RotateFlipType = (
    RotateNoneFlipNone = 0,
    Rotate90FlipNone   = 1,
    Rotate180FlipNone  = 2,
    Rotate270FlipNone  = 3,

    RotateNoneFlipX    = 4,
    Rotate90FlipX      = 5,
    Rotate180FlipX     = 6,
    Rotate270FlipX     = 7,

    RotateNoneFlipY    = Rotate180FlipX,
    Rotate90FlipY      = Rotate270FlipX,
    Rotate180FlipY     = RotateNoneFlipX,
    Rotate270FlipY     = Rotate90FlipX,

    RotateNoneFlipXY   = Rotate180FlipNone,
    Rotate90FlipXY     = Rotate270FlipNone,
    Rotate180FlipXY    = RotateNoneFlipNone,
    Rotate270FlipXY    = Rotate90FlipNone
  );
  TRotateFlipType = RotateFlipType;

  ColorAdjustType = (
    ColorAdjustTypeDefault,
    ColorAdjustTypeBitmap,
    ColorAdjustTypeBrush,
    ColorAdjustTypePen,
    ColorAdjustTypeText,
    ColorAdjustTypeCount,
    ColorAdjustTypeAny
  );
  TColorAdjustType = ColorAdjustType;

  ColorMatrix = packed array[0..4, 0..4] of Single;
  TColorMatrix = ColorMatrix;
  PColorMatrix = ^TColorMatrix;

  ColorMatrixFlags = (
    ColorMatrixFlagsDefault,
    ColorMatrixFlagsSkipGrays,
    ColorMatrixFlagsAltGray
  );
  TColorMatrixFlags = ColorMatrixFlags;

  CombineMode = (
    CombineModeReplace,
    CombineModeIntersect,
    CombineModeUnion,
    CombineModeXor,
    CombineModeExclude,
    CombineModeComplement
  );
  TCombineMode = CombineMode;

  FillMode = (
    FillModeAlternate,
    FillModeWinding
  );
  TFillMode = FillMode;

	GpGraphics = Pointer;
  GpBitmap = Pointer;
  GpRect = PGPRect;
  GpPoint = PGPPoint;
  GpPath = Pointer;
  GpFillMode = TFillMode;

	ARGB = DWORD;
  GpUnit = TUnit;
  GpImageAttributes = Pointer;

const
{
  ImageLockModeRead         = $0001;
  ImageLockModeWrite        = $0002;
  ImageLockModeUserInputBuf = $0004;
}
  PixelFormatIndexed     = $00010000;
  PixelFormatGDI         = $00020000;
  PixelFormatAlpha       = $00040000;
  PixelFormatPAlpha      = $00080000;
  PixelFormatExtended    = $00100000;
  PixelFormatCanonical   = $00200000;

  PixelFormatUndefined      = 0;
  PixelFormatDontCare       = 0;
  PixelFormat1bppIndexed    = (1  or ( 1 shl 8) or PixelFormatIndexed or PixelFormatGDI);
  PixelFormat4bppIndexed    = (2  or ( 4 shl 8) or PixelFormatIndexed or PixelFormatGDI);
  PixelFormat8bppIndexed    = (3  or ( 8 shl 8) or PixelFormatIndexed or PixelFormatGDI);
  PixelFormat16bppGrayScale = (4  or (16 shl 8) or PixelFormatExtended);
  PixelFormat16bppRGB555    = (5  or (16 shl 8) or PixelFormatGDI);
  PixelFormat16bppRGB565    = (6  or (16 shl 8) or PixelFormatGDI);
  PixelFormat16bppARGB1555  = (7  or (16 shl 8) or PixelFormatAlpha or PixelFormatGDI);
  PixelFormat24bppRGB       = (8  or (24 shl 8) or PixelFormatGDI);
  PixelFormat32bppRGB       = (9  or (32 shl 8) or PixelFormatGDI);
  PixelFormat32bppARGB      = (10 or (32 shl 8) or PixelFormatAlpha or PixelFormatGDI or PixelFormatCanonical);
  PixelFormat32bppPARGB     = (11 or (32 shl 8) or PixelFormatAlpha or PixelFormatPAlpha or PixelFormatGDI);
  PixelFormat48bppRGB       = (12 or (48 shl 8) or PixelFormatExtended);
  PixelFormat64bppARGB      = (13 or (64 shl 8) or PixelFormatAlpha  or PixelFormatCanonical or PixelFormatExtended);
  PixelFormat64bppPARGB     = (14 or (64 shl 8) or PixelFormatAlpha  or PixelFormatPAlpha or PixelFormatExtended);
  PixelFormatMax            = 15;

var
	IsGDIInit: Boolean = False;
	GDIToken: ULONG;

function GdiplusStartup(out token: ULONG; input: PGdiplusStartupInput; output: Pointer): Longint; stdcall; external 'gdiplus.dll' name 'GdiplusStartup';
procedure GdiplusShutdown(token: ULONG); stdcall; external 'gdiplus.dll' name 'GdiplusShutdown';

{$IFDEF ICM}
function GdipCreateBitmapFromFile(filename: PWCHAR; out bitmap: GPBITMAP): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromFileICM';
function GdipCreateBitmapFromStream(stream: ISTREAM; out bitmap: GPBITMAP): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromStreamICM';
{$ELSE}
function GdipCreateBitmapFromFile(filename: PWCHAR; out bitmap: GPBITMAP): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromFile';
function GdipCreateBitmapFromStream(stream: ISTREAM; out bitmap: GPBITMAP): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromStream';
{$ENDIF}
function GdipCreateBitmapFromHBITMAP(hbm: HBITMAP; hpal: HPALETTE; out bitmap: GPBITMAP): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromHBITMAP';
function GdipCreateHBITMAPFromBitmap(bitmap: GPBITMAP; out hbmReturn: HBITMAP; background: ARGB): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateHBITMAPFromBitmap';
//function GdipCreateBitmapFromGdiDib(gdiBitmapInfo: PBitmapInfo; gdiBitmapData: Pointer; out bitmap: GPBITMAP): GPSTATUS; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromGdiDib';
//function GdipCreateBitmapFromGraphics(width: Integer; height: Integer; target: GPGRAPHICS; out bitmap: GPBITMAP): GPSTATUS; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromGraphics';
function GdipCreateBitmapFromScan0(width: Integer; height: Integer; stride: Integer; format: PIXELFORMAT; scan0: PBYTE; out bitmap: GPBITMAP): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateBitmapFromScan0';
function GdipGetImageWidth(image: GPBITMAP; var width: UINT): Longint; stdcall; external 'gdiplus.dll' name 'GdipGetImageWidth';
function GdipGetImageHeight(image: GPBITMAP; var height: UINT): Longint; stdcall; external 'gdiplus.dll' name 'GdipGetImageHeight';
function GdipCloneBitmapAreaI(x: Integer; y: Integer; width: Integer; height: Integer; format: PIXELFORMAT; srcBitmap: GPBITMAP; out dstBitmap: GPBITMAP): Longint{Status}; stdcall; external 'gdiplus.dll' name 'GdipCloneBitmapAreaI';
//function GdipCloneImage(image: GPBITMAP; out cloneImage: GPBITMAP): GPSTATUS; stdcall; external 'gdiplus.dll' name 'GdipCloneImage';
function GdipGetImagePixelFormat(image: GPBITMAP; out format: TPIXELFORMAT): Longint; stdcall; external 'gdiplus.dll' name 'GdipGetImagePixelFormat';
//function GdipBitmapLockBits(bitmap: GPBITMAP; rect: GPRECT; flags: UINT; format: PIXELFORMAT; lockedBitmapData: PBITMAPDATA): GPSTATUS; stdcall; external 'gdiplus.dll' name 'GdipBitmapLockBits';
//function GdipBitmapUnlockBits(bitmap: GPBITMAP; lockedBitmapData: PBITMAPDATA): GPSTATUS; stdcall; external 'gdiplus.dll' name 'GdipBitmapUnlockBits';
function GdipDisposeImage(image: GPBITMAP): Longint; stdcall; external 'gdiplus.dll' name 'GdipDisposeImage';
function GdipGraphicsClear(graphics: GPGRAPHICS; color: ARGB): Longint; stdcall; external 'gdiplus.dll' name 'GdipGraphicsClear';
function GdipImageRotateFlip(image: GPBITMAP; rfType: ROTATEFLIPTYPE): Longint; stdcall; external 'gdiplus.dll' name 'GdipImageRotateFlip';
function GdipGetImageGraphicsContext(image: GPBITMAP; out graphics: GPGRAPHICS): Longint; stdcall; external 'gdiplus.dll' name 'GdipGetImageGraphicsContext';

function GdipCreateImageAttributes(out imageattr: GPIMAGEATTRIBUTES): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateImageAttributes';
function GdipDisposeImageAttributes(imageattr: GPIMAGEATTRIBUTES): Longint; stdcall; external 'gdiplus.dll' name 'GdipDisposeImageAttributes';
function GdipResetImageAttributes(imageattr: GPIMAGEATTRIBUTES; type_: COLORADJUSTTYPE): Longint; stdcall; external 'gdiplus.dll' name 'GdipResetImageAttributes';
function GdipSetImageAttributesColorMatrix(imageattr: GPIMAGEATTRIBUTES; type_: COLORADJUSTTYPE; enableFlag: Bool; colorMatrix: PCOLORMATRIX; grayMatrix: PCOLORMATRIX; flags: COLORMATRIXFLAGS): Longint; stdcall; external 'gdiplus.dll' name 'GdipSetImageAttributesColorMatrix';
function GdipSetImageAttributesColorKeys(imageattr: GPIMAGEATTRIBUTES; type_: COLORADJUSTTYPE; enableFlag: Bool; colorLow: ARGB; colorHigh: ARGB): Longint; stdcall; external 'gdiplus.dll' name 'GdipSetImageAttributesColorKeys';

function GdipSetClipPath(graphics: GPGRAPHICS; path: GPPATH; combineMode: COMBINEMODE): Longint; stdcall; external 'gdiplus.dll' name 'GdipSetClipPath';
function GdipResetClip(graphics: GPGRAPHICS): Longint; stdcall; external 'gdiplus.dll' name 'GdipResetClip';
function GdipCreatePath(brushMode: GPFILLMODE; out path: GPPATH): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreatePath';
function GdipDeletePath(path: GPPATH): Longint; stdcall; external 'gdiplus.dll' name 'GdipDeletePath';
function GdipResetPath(path: GPPATH): Longint; stdcall; external 'gdiplus.dll' name 'GdipResetPath';
function GdipAddPathPieI(path: GPPATH; x: Integer; y: Integer; width: Integer; height: Integer; startAngle: Single; sweepAngle: Single): Longint; stdcall; external 'gdiplus.dll' name 'GdipAddPathPieI';

function GdipCreateFromHDC(hdc: HDC; out graphics: GPGRAPHICS): Longint; stdcall; external 'gdiplus.dll' name 'GdipCreateFromHDC';
function GdipDrawImageRectRectI(graphics: GPGRAPHICS; image: GPBITMAP; dstx: Integer; dsty: Integer; dstwidth: Integer; dstheight: Integer; srcx: Integer; srcy: Integer; srcwidth: Integer; srcheight: Integer; srcUnit: GPUNIT; imageAttributes: GPIMAGEATTRIBUTES; callback: Pointer; callbackData: Pointer): Longint; stdcall; external 'gdiplus.dll' name 'GdipDrawImageRectRectI';
function GdipDrawImagePointsRectI(graphics: GPGRAPHICS; image: GPBITMAP; points: GPPOINT; count: Integer; srcx: Integer; srcy: Integer; srcwidth: Integer; srcheight: Integer; srcUnit: GPUNIT; imageAttributes: GPIMAGEATTRIBUTES; callback: Pointer; callbackData: Pointer): Longint; stdcall; external 'gdiplus.dll' name 'GdipDrawImagePointsRectI';
function GdipDrawImageRectI(graphics: GPGRAPHICS; image: GPBITMAP; x: Integer; y: Integer; width: Integer; height: Integer): Longint; stdcall; external 'gdiplus.dll' name 'GdipDrawImageRectI';
function GdipDeleteGraphics(graphics: GPGRAPHICS): Longint; stdcall; external 'gdiplus.dll' name 'GdipDeleteGraphics';

function GdipImageGetFrameDimensionsCount(image: GPBITMAP; var count: UINT): Longint; stdcall; external 'gdiplus.dll' name 'GdipImageGetFrameDimensionsCount';
function GdipImageGetFrameDimensionsList(image: GPBITMAP; dimensionIDs: PGUID; count: UINT): Longint; stdcall; external 'gdiplus.dll' name 'GdipImageGetFrameDimensionsList';
function GdipImageGetFrameCount(image: GPBITMAP; dimensionID: PGUID; var count: UINT): Longint; stdcall; external 'gdiplus.dll' name 'GdipImageGetFrameCount';
function GdipImageSelectActiveFrame(image: GPBITMAP; dimensionID: PGUID; frameIndex: UINT): Longint; stdcall; external 'gdiplus.dll' name 'GdipImageSelectActiveFrame';

function StringToWideString(const s: PAnsiChar): PWideChar;
var
  l: integer;
begin
  Result:= nil;
  if s = '' then Exit;

  l := MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, s, -1, nil, 0);
  if l > 1 then begin
    Result:= HeapAlloc(ISSpriteMain.hProcHeap, $9, l*2);
    MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, s, -1, Result, l);
  end;
end;

procedure GDIInitialize();
var
	GDIInp: TGdiplusStartupInput;
begin
	if not IsGDIInit then begin
    FillChar(GDIInp, SizeOf(GDIInp), 0);
		GDIInp.GdiplusVersion:=1;
		GDIInp.DebugEventCallback:= nil;
		GDIInp.SuppressBackgroundThread:= False;
		GDIInp.SuppressExternalCodecs:= False;

		GdiplusStartup(GDIToken, @GDIInp, nil);
    IsGDIInit:= True;
	end;	
end;

procedure GDIShutDown();
begin
	if IsGDIInit then begin
    GdiplusShutdown(GDIToken);
	  IsGDIInit:= False;
  end;
end;

function GDILoadFromFile(Filename: PAnsiChar): Pointer;
var
	TmpImg, tmpImg1: GPBITMAP;
  x, y, crc: Cardinal;
  aFile: PWideChar;
  pf: TPixelFormat;
  tmpBmp: HBITMAP;
begin
  Result:= nil;
  if not IsGDIInit then Exit;

  crc:= FileCRC(Filename);
  TmpImg:= MemGetImg(crc);
  if TmpImg<>nil then begin
    Result:= TmpImg;
    Exit;
  end;

  aFile:= StringToWideString(Filename);
	GdipCreateBitmapFromFile(aFile, TmpImg);
  GdipGetImagePixelFormat(TmpImg, pf);
  GdipGetImageWidth(TmpImg, x);
  GdipGetImageHeight(TmpImg, y);
  if (pf<=PixelFormat8bppIndexed) then begin
    GdipCreateHBITMAPFromBitmap(TmpImg, tmpBmp, $00000000);
    GdipCreateBitmapFromHBITMAP(tmpBmp, 0, TmpImg1);
    GdipCloneBitmapAreaI(0, 0, x, y, PixelFormat32bppPARGB, TmpImg1, Result);
    DeleteObject(TmpBmp);
    GdipDisposeImage(TmpImg);
    GdipDisposeImage(TmpImg1);
  end else begin
    GdipCloneBitmapAreaI(0, 0, x, y, PixelFormat32bppPARGB, TmpImg, Result);
    GdipDisposeImage(TmpImg);
  end;
  AddMemInfo(crc, Result);
  HeapFree(ISSpriteMain.hProcHeap, $9, aFile);
end;

function GDILoadFromBufer(lpBuffer: Pointer; lpBuffSize: Longint): Pointer;
var
  ImgStream: IStream;
  TmpImg: GPBITMAP;
  x, y, crc: Cardinal;
  LI, LU, LIN: Largeint;
  WR: Longint;
begin
  Result:= nil;
  if (not IsGDIInit)or(lpBuffer=nil)or(lpBuffSize=0) then Exit;

  crc:= BufferCRC(lpBuffer, lpBuffSize);
  TmpImg:= MemGetImg(crc);
  if TmpImg<>nil then begin
    Result:= TmpImg;
    Exit;
  end;

  CreateStreamOnHGlobal(0, true, ImgStream);
  LU:= lpBuffSize;
  ImgStream.SetSize(LU);
  FillChar(LI, Sizeof(Largeint), 0);
  ImgStream.Seek(LI, STREAM_SEEK_SET, LIN);
  imgStream.Write(lpBuffer, lpBuffSize, @WR);
  ImgStream.Seek(LI, STREAM_SEEK_SET, LIN);
  GdipCreateBitmapFromStream(ImgStream, TmpImg);
  GdipGetImageWidth(TmpImg, x);
  GdipGetImageHeight(TmpImg, y);
  GdipCloneBitmapAreaI(0, 0, x, y, PixelFormat32bppPARGB, TmpImg, Result);
  GdipDisposeImage(TmpImg);
  AddMemInfo(crc, Result);
//ImgStream.SetSize(0);
//ImgStream._Release;
end;