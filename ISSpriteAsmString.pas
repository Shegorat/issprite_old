function strcatL(dststr, srcstr: PAnsiChar): PAnsiChar;
asm
  push	esi
  push  edi
  push  ebx
  
  
  xor   ecx, ecx
@DstLen:  
  mov   bl, [eax+ecx]
  inc   ecx
  test  bl, bl
  jnz   @DstLen
  
  
  xor   esi, esi
@SrcLen:  
  mov   bl, [edx+esi]
  inc   esi
  test  bl, bl
  jnz   @SrcLen
  
  mov   edi, eax
  add   edi, esi

  
@ShiftDst:  
  dec   ecx
  mov   bl, [eax+ecx]
  mov   [edi+ecx], bl
  test  ecx, ecx
  jnz   @ShiftDst

  
@CatString:
  dec   esi
  mov   bl, [edx+esi]
  mov   [eax+esi], bl
  jnz   @CatString
  
  pop   ebx
  pop   edi
  pop   esi
end;

function strcatR(DstStr: PAnsiChar; SrcStr: PAnsiChar): PAnsiChar;
asm
  test  eax, eax
  jz    @exit
  test  edx, edx
  jz    @exit;
  
  push  ebx
  mov   ecx, eax

  xor   ebx, ebx

@DstLen:
  mov   bl, [eax]
  inc   eax
  test  bl, bl
  jz    @cpy
  jmp   @DstLen

@cpy:
  mov   bl, [edx]
  inc   edx
  mov   [eax], bl
  inc   eax
  test  bl, bl
  jz    @brk2
  jmp   @cpy

@brk2:
  mov   eax, ecx
  pop   ebx

@exit:
end;

function strcopy(DstStr, SrcStr: PAnsiChar; Count: DWORD): PAnsiChar;
asm
  test  eax, eax
  jz    @exit;
  test  edx, edx
  jz    @exit;
  test  ecx, ecx
  jz    @exit;
  
  push  ebx
  push  edi
  push  esi
  
  mov   esi, eax
  mov   edi, edx
  
  xor   ebx, ebx
  mov   [esi+ecx], bl
  
  mov   eax, ecx
  mov   edx, ecx
  and   edx, 3h
  shr   eax, 02h

  test  edx, edx
  jz    @cpy_s
  dec   edx
  jz    @cpy_1
  dec   edx
  jz    @cpy_2
  //jmp   @cpy_3
  
@cpy_3:
  mov   bl, [edi+eax*4+2]
  mov   [esi+eax*4+2], bl
  
@cpy_2:
  mov   bl, [edi+eax*4+1]
  mov   [esi+eax*4+1], bl 
  
@cpy_1:
  mov   bl, [edi+eax*4]
  mov   [esi+eax*4], bl

@cpy_s:
  test  eax, eax
  jz    @end

@cpy:
  dec   eax
  mov   ebx, [edi+eax*4]
  mov   [esi+eax*4], ebx
  jnz   @cpy

@end:  
  mov   eax, esi

  pop   esi
  pop   edi
  pop   ebx
@exit:  
end;

procedure strdel(Str: PAnsiChar; Pos, Count: Integer);
asm
  test  eax, eax
  jz    @exit
  test  ecx, ecx
  jz    @exit
  test  edx, edx
  jnz   @start
  inc   edx
  
@start:  
  push  esi
  push  edi
  push  ebx
  
  dec   edx

  add   eax, edx
  mov   esi, eax
  add   esi, ecx
  
  xor   ebx, ebx
  xor   edi, edi
@cpy:
  mov   bl, [esi+edi]
  mov   [eax+edi], bl
  inc   edi
  test  bl, bl
  jnz   @cpy

  pop   ebx
  pop   edi
  pop   esi
  
@exit:  
end;

function StrPos(SrcStr, SubStr: PAnsiChar): Integer;
asm
  test  eax, eax
  jz    @exit
  test  edx, edx
  jz    @exit
  
  push  ebx
  push  edi
  sub   esp, 0008h
  
  xor   edi, edi
  xor   ebx, ebx
@SrcLen:  
  mov   bl, [eax+edi]
  inc   edi
  test  bl, bl
  jnz   @Srclen
  
  mov   [esp+04h], edi
  
  xor   edi, edi
@SubLen:
  mov   bl, [edx+edi]
  inc   edi
  test  bl, bl
  jnz   @SubLen
  
  mov   [esp], edi
  
  xor  ecx, ecx
@fullcmp:
  
  xor   edi, edi
  @cmpstr:
    mov   bl, [edx+edi]
    test  bl, bl
    jz    @Res
    cmp   bl, byte ptr [eax+edi]
    jne   @next
    inc   edi
    jmp   @cmpstr

  @Res:
    mov   eax, ecx
    inc   eax
    jmp   @break
    
@next:  
  inc   ecx  
  inc   eax

  mov   edi, [esp+04h]
  sub   edi, ecx
  cmp   edi, [esp]
  jge   @fullcmp

  xor   eax, eax
  
@break:  
  add   esp, 0008h
  pop   edi
  pop   ebx

@exit:  
end;

function strlen(SrcStr: PAnsiChar): Integer;
asm
  test  eax, eax
  jz    @exit
  
  xor   edx, edx
  mov   ecx, eax
  xor   eax, eax

@StrLen:
  mov   dl, [ecx]
  inc   ecx
  test  dl, dl
  jz    @exit
  inc   eax
  jmp   @StrLen

@exit:  
end;