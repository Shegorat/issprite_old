const
  PTR_SLASH : array [0..3] of AnsiChar = '...\';
  DIR_SLASH : array [0..1] of AnsiChar = ':\';
  TXT_SLASH : AnsiChar = '\';
  TXT_SPACE : AnsiChar = ' ';

function Cut(var S: String; Pos, Length: Integer): String;
begin
  Result:= Copy(S, Pos, Length);
  Delete(S, Pos, Length);
end;

function FindSpace(s: PAnsiChar; Min, Max: Integer): Integer;
var
  n, l: integer;
begin
  Result:= 0;
  l:= strlen(s);
  if (Max=0)or(l=0) then Exit;
  if Max>l then Max:=l;
  if Min<=0 then Min:=1;
  n:= Max;
  repeat
    if s[n-1]=TXT_SPACE then begin
      Result:= n;
      Break;
    end;
    Dec(n);
  until n<=min;
end;

function GetTextWidth(aDC: HDC; aFont: HFONT; Text: PAnsiChar): Integer;
var
  aSize: TSize;
  OldObj: HFONT;
  abc1, abc2: ABC;
  lfnt: TLogFont;
  l: Integer;
begin
  Result:= 0;
  l:= strlen(Text);
  if l=0 then Exit;

  FillChar(lfnt, SizeOf(TLogFont), 0);
  GetObject(aFont, SizeOf(TLogFont), @lfnt);
  OldObj:= SelectObject(aDC, aFont);
  GetTextExtentPoint32(aDC, Text, l, aSize);
  if (Length(Text)<>1)and(lfnt.lfItalic=1) then begin
    GetCharABCWidths(aDC, Ord(Text[0]), Ord(Text[0]), abc1);
    GetCharABCWidths(aDC, Ord(Text[l-1]), Ord(Text[l-1]), abc2);
    if abc1.abcA<0 then aSize.cx:= aSize.cx-abc1.abcA;
    if abc2.abcC<0 then aSize.cx:= aSize.cx-abc2.abcC
  end;
  SelectObject(aDC, OldObj);

  Result:= aSize.cx;
end;

function GetTextHeight(aDC: HDC; aFont: HFONT; Text: PAnsiChar): Integer;
var
  aSize: TSize;
  OldObj: HFONT;
begin
  Result:= 0;
  If Text='' then Exit;

  OldObj:= SelectObject(aDC, aFont);
  GetTextExtentPoint32(aDC, Text, strlen(Text), aSize);
  SelectObject(aDC, OldObj);

  Result:= aSize.cy;
end;

function TrimEx(const S: String): String;
var
  I, L: Integer;
begin
  Result:= '';
  if S='' then Exit;
  I:= 1;
  L:= Length(S);
  while (I <= L) and (S[I] = TXT_SPACE) do Inc(I);
  while (L >= I) and(S[L] = TXT_SPACE) do Dec(L);
  if (L-I)>=0 then
    Result:= Copy(S, I, L-I+1);
end;

function AnsiLowerCase(const S: string): string;
var
  Len: Integer;
begin
  Len := Length(S);
  SetString(Result, PChar(S), Len);
  if Len > 0 then CharLowerBuff(Pointer(Result), Len);
end;

function AnsiUpperCase(const S: string): string;
var
  Len: Integer;
begin
  Len := Length(S);
  SetString(Result, PChar(S), Len);
  if Len > 0 then CharUpperBuff(Pointer(Result), Len);
end;

function AnsiIndexText(const AText: String; const AValues: array of PAnsiChar): Integer;
var
  I, hi: Integer;
begin
  Result := -1;
  i:= Low(AValues);
  hi:= High(AValues);
  repeat
    if ((CompareString(LOCALE_USER_DEFAULT, NORM_IGNORECASE, PAnsiChar(AText), Length(AText), AValues[I], Length(AValues[I])) - 2)=0) then
    begin
      Result := I;
      Break;
    end;
    Inc(i);
  until i>hi;
end;

function StrToInt(const S: string): Integer;
var
  E: Integer;
begin
  Val(S, Result, E);
  if E <> 0 then Result:= 0;
end;

function MinimizePathName(aDC: HDC; const Filename: String; Font: HFont; MaxLen: Integer): String;
var
  Drive, Dir, Name: String;
  k, n, l: Integer;
  FirstPass: Boolean;
begin
  Result := FileName;
  k:= Pos(DIR_SLASH, Filename);
  if k<=0 then Exit;

  Drive:= Copy(Filename, 1, k+1);

  n:= Length(Filename);
  l:= n;

  while Filename[n]<>TXT_SLASH do
    Dec(n);
  Name:= Copy(Filename, n+1, l-n);
  Dir:= Copy(Filename, k+1, n-k);

  FirstPass:= True;

  while ((Dir <> '') or (Drive <> '')) and (GetTextWidth(aDC, Font, PAnsiChar(Result)) > MaxLen) do
  begin
    if Dir<>'' then begin
      l:= Length(Dir);
      if FirstPass then
        n:= 1
      else begin
        if l=4 then begin
          Dir:= '';
          n:= -1;
        end else
          n:= 5;
      end;

      if n<>-1 then begin
        while (Dir[n]<>TXT_SLASH)and(n<>l) do
          Inc(n);

        Delete(Dir, 1, n);
      end;

      FirstPass:= False;
      if Dir<> '' then
        Dir:= PTR_SLASH+Dir;
    end else begin
      Dir:= PTR_SLASH;
      Drive:= '';
    end;

    Result := Drive + Dir + Name;
  end;
end;