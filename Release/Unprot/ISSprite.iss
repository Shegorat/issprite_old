[code]
type
  TImgButtonEvent = procedure(hWnd: HWND; nEventID: Word);
  
const
  SHD_GRADIENTVER     = $0020;
  SHD_GRADIENTHOR     = $0010;

  SHD_STYLEGLOW       = $0080;
  SHD_STYLESHADOW     = $0040;
  
  btnMouseClickEvent  = 1;
  btnMouseEnterEvent  = 2;
  btnMouseLeaveEvent  = 3;
  btnMouseMoveEvent   = 4;
  btnMouseDownEvent   = 5;
  btnMouseUpEvent     = 6;
  
  btnAlignDefault     = 1;
  btnAlignTopLeft     = 2;
  btnAlignBottomLeft  = 3;
  btnAlignTopRight    = 4;
  btnAlignBottomRight = 5;
  btnAlignTopCenter   = 6;
  btnAlignBottomCenter= 7;
  btnAlignCenter      = 8;
  
  BTN_PANEL           = $10;
  BTN_BUTTON          = $20;
  BTN_CHECKBOX        = $40;
  BTN_CHECKBOXEX      = $80;
  
function spImgLoadImage(lpParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'spImgLoadImage@files:ISSprite.dll stdcall';
//������� bmploadImg ��������� BMP-����������� � ������
//AParent         - ����� ����, ��� ����� ���������� �����������
//Filename        - ���� �� ������������ �����������
//ALeft, ATop     - ���������� �������� ������ ���� ������ ����������� (� ����������� ���������� ������� Wnd)
//AWidth, AHeight - ������, ������ �����������
//BColor          - ����, ������� �� ����� ��������� ������ �����������, ���� �� ��������� �������� ������ ���� -1
//Stretch         - �������������� ����������� ��� ���
//IsBkg           - ���� True, �� ������ ����������� �� ���� �����, ����� ��� VCL-�������� ���� TLabel, TPanel � �.�
//                - ���������� �������� ����������� � IsBkg = False
//������������ �������� - ����� �����������, ������������ � ��������� �������� ��� ������ � BMP

function spImgLoadImageFromBuffer(lpParent: HWND; lpBuffer: PAnsiChar; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
  external 'spImgLoadImageFromBuffer@files:ISSprite.dll stdcall';
  
procedure spImgRelease(Img: Longword);
  external 'spImgRelease@files:ISSprite.dll stdcall';
//������� ����������� �� ������
//Img             - ����� �����������

function spImgCreateFormFromImage(hWnd: HWND; hImage: Longword; nSetTimer: Boolean): Boolean;
  external 'spImgCreateFormFromImage@files:IsSprite.dll stdcall';
//������� ����� �� �����������, �� ����� ����� �� ����� ������������ ����������� ��������
//����� ���, ��� ���� ��������� � ������� bmpAddImageFormControl
//����� ����� ����� ����� ����� ������� �������������� � ������� bmpUpdateImageForm
//����������: ������������ ���������� �������� ���� - 8 ��.
//hWnd            - ����� �����
//hImage          - ����� �����������, ������������ ����� bmpLoadImg

procedure spImgUpdateImageForm(hWnd: HWND);
  external 'spImgUpdateImageForm@files:IsSprite.dll stdcall';
//��������� �����, ��������� �� ������ �����������
//hWnd            - ����� �����

procedure spImgSetImageFormAlpha(hWnd: HWND; lpAlpha: Byte);
  external 'spImgSetImageFormAlpha@files:IsSprite.dll stdcall';
  
procedure spImgGetDimensions(Img: longword; var Width, Height: Integer);
  external 'spImgGetDimensions@files:ISSprite.dll stdcall';
//���������� ������������ ������, ������ �����������
//Img             - ����� �����������, ��� �������� ����� �������� ������������ ������ � ������
//Width           - ��������� �� ����������, � ������� ����� ���������� ������ �����������
//Height          - ��������� �� ����������, � ������� ����� ���������� ������ �����������
//                  ��� ������� ���������� ������, ������ ������ �������

procedure spImgSetGifInterval(hImg: Longword; iTime: Longword);
  external 'spImgSetGifInterval@files:ISSprite.dll stdcall';
  
procedure spImgSetGifIdleTime(hImg: Longword; iTime: Longword);
  external 'spImgSetGifIdleTime@files:ISSprite.dll stdcall';
  
procedure spImgSetRotateAngle(hImg: Longword; lpAngle: Integer);
  external 'spImgSetRotateAngle@files:ISSprite.dll stdcall';
  
function spImgGetRotateAngle(hImg: Longword): Integer;
  external 'spImgGetRotateAngle@files:ISSprite.dll stdcall';
  
procedure spImgSetBackgroundColor(Img: longword; aColor: integer);
  external 'spImgSetBackgroundColor@files:ISSprite.dll stdcall';
//������������� "���������� ����" ��� �����������
//Img             - ����� �����������
//aColor          - ����, ������� �� ����� ��������� ������ �����������, ���� �� ��������� �������� ������ ���� -1

procedure spImgSetTransparent(Img: Longword; aAlpha: Integer);
  external 'spImgSetTransparent@files:ISSprite.dll stdcall';
//������� ��������� ������� ������������ ��� �����������
//Img             - ����� �����������
//aAlpha          - ������ ������������ ���������� �������� 0 - 255
//                  0 - ��������� ����������, 255 - ��������� ������������

function spImgGetTransparent(Img: Longword): Integer;
  external 'spImgGetTransparent@files:ISSprite.dll stdcall';
//���������� ������� ������������ �����������
//Img             - ����� �����������
//������������ �������� �� 0 �� 255

//procedure spImgSetParent(Img: Longword; h: HWND);
//  external 'spImgSetParent@files:ISSprite.dll stdcall';
//������������� ����� �������� ��� �����������
//Img             - ����� �����������, � �������� ����� ������� ��������
//h               - ����� ������ ��������

function spImgGetParent(Img: Longword): HWND;
  external 'spImgGetParent@files:ISSprite.dll stdcall';
//���������� ������� �������� �����������
//Img             - ����� �����������, �������� �������� ����� ��������
//                  ���� ����������� �� ������� ���������� INVALID_HANDLE_VALUE

procedure spImgSetVisibility(Img: longword; Visible: Boolean);
  external 'spImgSetVisibility@files:ISSprite.dll stdcall';
//������������� �������� ��������� �����������
//Img             - ����� �����������, ��� �������� ����� ���������� �������� ���������
//Visible         - �������� ������� ����� ���������

function spImgGetVisibility(Img: longword): Boolean;
  external 'spImgGetVisibility@files:ISSprite.dll stdcall';
//���������� �������� ��������� �����������
//Img             - ����� �����������, ��� �������� ����� �������� �������� ���������

procedure spImgSetPos(Img: Longword; ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgSetPos@files:ISSprite.dll stdcall';
//��������� ��������� ����������� �� �����
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �����������

procedure spImgGetPos(Img: Longword; var ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgGetPos@files:ISSprite.dll stdcall';
//���������� ��������� ����������� �� �����
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �����������

procedure spImgSetVisiblePart(Img: Longword; ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgSetVisiblePart@files:ISSprite.dll stdcall';
//������������� ������� ����� ������ �����������
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �������� �����
//                  ���� Stretch=False �� ������������, ��������� ����� ����������� � ������� � ������� ������
//                  ������ � ������ ������������� �������� bmpSetPos

procedure spImgGetVisiblePart(Img: Longword; var ALeft, ATop, AWidth, AHeight: Integer);
  external 'spImgGetVisiblePart@files:ISSprite.dll stdcall';
//���������� ������� ����� ������ �����������
//Img             - ����� �����������
//Aleft, ATop     - ���������� �������� ������ ����
//AWidth, Aheight - ������, ������ �������� �����

procedure spImgSetVisiblePie(Img: Longword; lpVisAngle: Integer);  //!! not working yet !!
  external 'spImgSetVisiblePie@files:ISSprite.dll stdcall';
  
procedure spImgSetSpriteCount(Img: Longword; SpriteCount: Integer);
  external 'spImgSetSpriteCount@files:ISSprite.dll stdcall';
//��� ������ ������������� ����, ��� ����������� �������� ��������, ������������� ������������ ������ �������
//Img             - ����� �����������, ��� �������� ����� ���������� ������������ ������ �������
//SpriteCount     - ����� �������� � �����, �� �� ������������ ������
//                  ������� �������� ��� ��� ������������ �������� ����������� ����� ������������ �����������
//                  ����� ������� �������� ��, ��� ������ ������ ���� �����������, �.� ��� ����������� ������
//                  ������������� � �������

function spImgGetSpriteCount(Img: Longword): Integer;
  external 'spImgGetSpriteCount@files:ISSprite.dll stdcall';
//���������� ������� ������ �������
//Img             - ����� �����������, ��� �������� ����� ������ ������ �������

procedure spImgSetSpriteIndex(Img: Longword; Index: Integer);
  external 'spImgSetSpriteIndex@files:ISSprite.dll stdcall';
//������������� ������ �������
//Img             - ����� �����������, ��� �������� ����� ���������� ������ �������
//Index           - ���������� ��� ������, ��������� �������� �� 1 �� ������������� �������
//                  ��� ������ �� ������� ���������� �������� ������������� ������������� �� ���������� ��������

function spImgGetSpriteIndex(Img: Longword): Integer;
  external 'spImgGetSpriteIndex@files:ISSprite.dll stdcall';
//���������� ������������ ������ �������, �������� ������ ����� ������ bmpSetProperties
//Img             - ����� �����������, ��� �������� ����� ������ ������������ ������

procedure spApplyChanges(h: HWND);
  external 'spApplyChanges@files:ISSprite.dll stdcall';
//��������� �������� ����������� �� �����
//h               - ����� �����, �� ������� ���������� ������������ �����������

procedure spInitialize(nUseLogicPixels: Boolean; nUseSmartPaint: Boolean);
  external 'spInitialize@files:ISSprite.dll stdcall';
//����������� �������� ����� �������������� ����������

procedure spShutdown();
  external 'spShutdown@files:ISSprite.dll stdcall';
//����������� �������� ��� ���������� ����������

function spShdAddText(lpParent: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpCaption: PAnsiChar; lpTextColor: Integer; aFont: Longword): Longword;
  external 'spShdAddText@files:ISSprite.dll stdcall';
  
procedure spShdSetText(hText: Longword; lpCaption: PAnsiChar);
  external 'spShdSetText@files:ISSprite.dll stdcall';

procedure spShdSetShadow(hText: Longword; lpShadowColor: Cardinal; lpShadowSize, lpShadowIntense: Byte; lpShadowStyle: DWORD);
  external 'spShdSetShadow@files:ISSprite.dll stdcall';

procedure spShdSetRotateAngle(hText: Longword; lpAngle: Integer);
  external 'spShdSetRotateAngle@files:ISSprite.dll stdcall';
  
procedure spShdSetVisible(hText: Longword; lpVisible: Boolean);
  external 'spShdSetVisible@files:ISSprite.dll stdcall';
  
function spShdGetVisible(hText: Longword): Boolean;
  external 'spShdGetVisible@files:ISSprite.dll stdcall';

procedure spShdAddGradient(hText: Longword; lpBeginColor, lpEndColor: Cardinal; lpStyle: DWORD);
  external 'spShdAddGradient@files:ISSprite.dll stdcall';
  
procedure spShdFreeGradient(hText: Longword);
  external 'spShdFreeGradient@files:ISSprite.dll stdcall';

procedure spShdSetTextColor(hText: Longword; lpTextColor: Cardinal);
  external 'spShdSetTextColor@files:ISSprite.dll stdcall';
  
procedure spShdSetShadowColor(hText: Longword; lpShadowColor: Cardinal);
  external 'spShdSetShadowColor@files:ISSprite.dll stdcall';
  
procedure spShdSetCharacterExtra(hText: Longword; lpCharExtra: Integer);
  external 'spShdSetCharacterExtra@files:ISSprite.dll stdcall';
  
procedure spShdSetPos(hText: Longword; lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spShdSetPos@files:ISSprite.dll stdcall';

procedure spShdGetPos(hText: Longword; var lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spShdGetPos@files:ISSprite.dll stdcall';

//Fonts
function spFntCreateFont(lpFontName: PAnsiChar; lpBold, lpItalic, lpUnderline, lpStrikeOut: Boolean; lpFontSize: Integer): Longword;
  external 'spFntCreateFont@files:ISSprite.dll stdcall';

function spFntCreateFont2(lpFontName: PAnsiChar; lpWBold: Integer; lpItalic, lpUnderline, lpStrikeOut: Boolean; lpFontSize: Integer): Longword;
  external 'spFntCreateFont2@files:ISSprite.dll stdcall';
  
function spFntFreeFont(lpFont: Longword): Boolean;
  external 'spFntFreeFont@files:ISSprite.dll stdcall';

function spFntRegisterFont(lpFilename: PAnsiChar): Boolean;
  external 'spFntRegisterFont@files:ISSprite.dll stdcall';

function spFntUnregisterFont(lpFilename: PAnsiChar): Boolean;
  external 'spFntUnregisterFont@files:ISSprite.dll stdcall';
//Buttons
function WrapButtonProc(callback: TImgButtonEvent; Paramcount: Integer): Longword;
  external 'wrapcallbackaddr@files:callbackctrl.dll stdcall';

function spBtnCreateButton(hParent: HWND; lpFilename: PAnsiChar; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'spBtnCreateButton@files:ISSprite.dll stdcall';
  
function spBtnCreateButtonFromBuffer(hParent: HWND; lpBuffer: PAnsiChar; lpBuffSize: Longint; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
  external 'spBtnCreateButtonFromBuffer@files:ISSprite.dll stdcall';
  
procedure spBtnRefresh(hBtn: HWND);
  external 'spBtnRefresh@files:ISSprite.dll stdcall';

procedure spBtnSetCursor(hBtn: HWND; lpCursor: Longword);
  external 'spBtnSetCursor@files:ISSprite.dll stdcall';

function spBtnGetSysCursor(lpCursorID: Integer): Longword;
  external 'spBtnGetSysCursor@files:ISSprite.dll stdcall';

procedure spBtnSetFont(hBtn: HWND; lpFont: Longword);
  external 'spBtnSetFont@files:ISSprite.dll stdcall';
  
procedure spBtnSetFontColor(hBtn: HWND; lpColorDefault, lpColorHover, lpColorPressed, lpColorDisabled: Cardinal);
  external 'spBtnSetFontColor@files:ISSprite.dll stdcall';

procedure spBtnSetText(hBtn: HWND; lpText: PAnsiChar);
  external 'spBtnSetText@files:ISSprite.dll stdcall';

procedure spBtnSetTextAlignment(hBtn: HWND; lpAlign: DWORD);
  external 'spBtnSetTextAlignment@files:ISSprite.dll stdcall';

procedure spBtnSetTextIndent(hBtn: HWND; iLeft, iTop: Integer);
  external 'spBtnSetTextIndent@files:ISSprite.dll stdcall';
  
procedure spBtnSetEvent(hBtn: HWND; lpEventID: Integer; lpEvent: Longword);
  external 'spBtnSetEvent@files:ISSprite.dll stdcall';

procedure spBtnSetPos(hBtn: HWND; lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spBtnSetPos@files:ISSprite.dll stdcall';

procedure spBtnGetPos(hBtn: HWND; var lpLeft, lpTop, lpWidth, lpHeight: Integer);
  external 'spBtnGetPos@files:ISSprite.dll stdcall';

procedure spBtnSetEnabled(hBtn: HWND; lpEnabled: Boolean);
  external 'spBtnSetEnabled@files:ISSprite.dll stdcall';

function spBtnGetEnabled(hBtn: HWND): Boolean;
  external 'spBtnGetEnabled@files:ISSprite.dll stdcall';

procedure spBtnSetVisible(hBtn: HWND; lpVisible: Boolean);
  external 'spBtnSetVisible@files:ISSprite.dll stdcall';

function spBtnGetVisible(hBtn: HWND): Boolean;
  external 'spBtnGetVisible@files:ISSprite.dll stdcall';

procedure spBtnSetChecked(hBtn: HWND; lpChecked: Byte);
  external 'spBtnSetChecked@files:ISSprite.dll stdcall';

function spBtnGetChecked(hBtn: HWND): Byte;
  external 'spBtnGetChecked@files:ISSprite.dll stdcall';

procedure spBtnSetTransparent(hBtn: HWND; lpAlpha: Byte);
  external 'spBtnSetTransparent@files:ISSprite.dll stdcall';

function spBtnGetTransparent(hBtn: HWND): Byte;
  external 'spBtnGetTransparent@files:ISSprite.dll stdcall';

procedure MoveSpriteHor(Img: Longword; Step, MinLeft, MaxLeft: Integer);
var
  x, y, x2, y2, ox, oy, px, py, px2, py2: Integer;
begin
  spImgGetPos(Img, x, y, x2, y2);
  spImgGetDimensions(Img, ox, oy);
  spImgGetVisiblePart(Img, px, py, px2, py2);
  x:=x+Step;
  if x2=0 then if Step<0 then x:= MaxLeft else x:= MinLeft;

  if Step>0 then begin
    if ((x+x2)>MaxLeft)and(x2>0) then begin
      px2:=px2-Step;
    end;
    if (x=MinLeft)and(x2<ox) then begin
      px2:=px2+Step;
      px:=ox-px2;
      x:=x-Step
    end;
  end else begin
    if (x<MinLeft)and(x2>0) then begin
      px2:=px2+Step;
      px:=ox-px2;
      x:=x-Step;
    end;
    if (x+ox>=MaxLeft)and(x2<ox) then begin
      px:=0;
      px2:=px2-Step;
    end;
  end;
  x2:=px2;
  spImgSetVisiblepart(Img, px, py, px2, py2);
  spImgSetPos(Img, x, y, x2, y2);
end;

function IsButtonChecked(hBtn: HWND): Boolean;
begin
  Result:= (spBtnGetChecked(hBtn) > 0);
end;

function ImgLoadFromBuffer(lpParent: HWND; lpFilename: String; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStretch, lpIsBkg: Boolean): Longword;
var
  buffer: AnsiString;
  FileSize: Longint;
begin
  lpFilename:= ExtractFileName(lpFilename);
  FileSize:= ExtractTemporaryFileSize(lpFilename);
  SetLength(Buffer, FileSize);
  #ifdef UNICODE
    ExtractTemporaryFileToBuffer(lpFilename, CastAnsiStringToInteger(Buffer));
  #else
    ExtractTemporaryFileToBuffer(lpFilename, CastStringToInteger(Buffer));
  #endif
  Result:= spImgLoadImageFromBuffer(lpParent, PAnsiChar(buffer), FileSize, lpLeft, lpTop, lpWidth, lpHeight, lpStretch, lpIsBkg);
  SetLength(buffer, 0);
end;

function BtnLoadFromBuffer(hParent: HWND; lpFilename: String; lpLeft, lpTop, lpWidth, lpHeight: Integer; lpStyle: DWORD; lpShadowSize: Integer): HWND;
var
  buffer: AnsiString;
  FileSize: Longint;
begin
  lpFilename:= ExtractFileName(lpFilename);
  FileSize:= ExtractTemporaryFileSize(lpFilename);
  SetLength(Buffer, FileSize);
  #ifdef UNICODE
    ExtractTemporaryFileToBuffer(lpFilename, CastAnsiStringToInteger(Buffer));
  #else
    ExtractTemporaryFileToBuffer(lpFilename, CastStringToInteger(Buffer));
  #endif
  Result:= spBtnCreateButtonFromBuffer(hParent, PAnsiChar(buffer), FileSize, lpLeft, lpTop, lpWidth, lpHeight, lpStyle, lpShadowSize);
  SetLength(buffer, 0);
end;