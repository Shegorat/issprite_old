{$ifdef RELEASE}
function sp301(lpFontName: PAnsiChar; lpBold: Longint; lpItalic, lpUnderline, lpStrikeOut: Boolean; lpFontSize: Integer): HFONT; stdcall;
{$else}
function spFntCreateFont(lpFontName: PAnsiChar; lpBold: Longint; lpItalic, lpUnderline, lpStrikeOut: Boolean; lpFontSize: Integer): HFONT; stdcall;
{$endif}
var
  lfnt: TLogFont;
begin
  FillChar(lfnt, SizeOf(TLogFont), 0);
	
	with lfnt do begin
		if ISSpriteMain.spNeedLPY then
			lfHeight:= -MulDiv(lpFontSize, ISSpriteMain.LPixelsY, 72)
		else
			lfHeight:= -lpFontSize;
			
		if lpBold = 1 then lfWeight:= FW_BOLD
			else if lpBold = 0 then lfWeight:= FW_NORMAL
		else
			lfWeight         := lpBold;
			lfItalic         := Byte(lpItalic);
			lfUnderline      := Byte(lpUnderline);
			lfStrikeOut      := Byte(lpStrikeOut);
			lfCharSet        := DEFAULT_CHARSET;
			lfOutPrecision   := OUT_OUTLINE_PRECIS;
			lfPitchAndFamily := VARIABLE_PITCH;
			lfQuality        := CLEARTYPE_QUALITY;
			
			StrCopy(lfFaceName, lpFontName, StrLen(lpFontName));
			Result:= CreateFontIndirect(lfnt);
	end;
end;

{$ifdef RELEASE}
procedure sp302(var lpFont: HFONT; lpCharSet: DWORD); stdcall;
{$else}
procedure spFntChangeCharSet(var lpFont: HFONT; lpCharSet: DWORD); stdcall;
{$endif}
var
  OldFnt: HFONT;
  lfnt: TLogFont;
begin
  FillChar(lfnt, SizeOf(TLogFont), 0);
  GetObject(lpFont, SizeOf(TLogFont), @lfnt);
  OldFnt:= lpFont;
  lfnt.lfCharSet:= lpCharSet;
  lpFont:= CreateFontIndirect(lfnt);
  DeleteObject(OldFnt);
end;

{$ifdef RELEASE}
function sp303(lpFilename: PAnsiChar): Boolean; stdcall;
{$else}
function spFntRegisterFont(lpFilename: PAnsiChar): Boolean; stdcall;
{$endif}
begin
  Result:= AddFontResourceEx(lpFilename, FR_PRIVATE, nil) > 0;
end;

{$ifdef RELEASE}
function sp304(lpBuffer: Pointer; lpBuffSize: Longint): HWND; stdcall;
{$else}
function spFntRegisterMemFont(lpBuffer: Pointer; lpBuffSize: Longint): HWND; stdcall;
{$endif}
var
  pcFonts: DWORD;
begin
  pcFonts:= 0;
  Result:= AddFontMemResourceEx(lpBuffer, lpBuffSize, nil, @pcFonts);
  if pcFonts = 0 then Result:= INVALID_HANDLE_VALUE;
end;

{$ifdef RELEASE}
function sp305(lpFilename: PAnsiChar): Boolean; stdcall;
{$else}
function spFntUnregisterFont(lpFilename: PAnsiChar): Boolean; stdcall;
{$endif}
begin
  Result:= RemoveFontResourceEx(lpFilename, FR_PRIVATE, nil);
end;

{$ifdef RELEASE}
function sp306(hFont: HWND): Boolean; stdcall;
{$else}
function spFntUnregisterMemFont(hFont: HWND): Boolean; stdcall;
{$endif}
begin
  Result:= RemoveFontMemResourceEx(hFont);
end;

{$ifdef RELEASE}
function sp307(lpFont: HFONT): Boolean; stdcall;
{$else}
function spFntFreeFont(lpFont: HFONT): Boolean; stdcall;
{$endif}
begin
  Result:= DeleteObject(lpFont);
end;

exports
{$ifdef RELEASE}
	sp301, sp302, sp303, sp304, sp305, sp306, sp307;
{$else}
	spFntCreateFont,
	spFntChangeCharSet,
	spFntRegisterFont,
	spFntRegisterMemFont,
	spFntUnregisterFont,
	spFntUnregisterMemFont,
	spFntFreeFont;
{$endif}